package com.ruoyi.loyj.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 抖音照片对象 loyj_douyin_photo
 *
 * @author loyj
 * @date 2021-10-30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DouyinPhoto
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 抖音外键ID */
    @Excel(name = "抖音外键ID")
    private String douyinId;

    /** 照片链接 */
    @Excel(name = "照片链接")
    private String photoUrl;
}
