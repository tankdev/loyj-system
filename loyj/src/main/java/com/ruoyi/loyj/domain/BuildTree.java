package com.ruoyi.loyj.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Treeselect树结构实体类
 *
 * @author ruoyi
 */
public class BuildTree implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 节点ID */
    private String id;

    /** 节点名称 */
    private String label;

    /** 子节点 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<BuildTree> children;

    public BuildTree()
    {

    }

    public BuildTree(DouyinClass dys)
    {
        this.id = dys.getId();
        this.label = dys.getDyClassName();
        this.children = dys.getChildren().stream().map(BuildTree::new).collect(Collectors.toList());
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public List<BuildTree> getChildren()
    {
        return children;
    }

    public void setChildren(List<BuildTree> children)
    {
        this.children = children;
    }
}
