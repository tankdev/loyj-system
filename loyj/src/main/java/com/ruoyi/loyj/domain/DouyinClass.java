package com.ruoyi.loyj.domain;

import com.ruoyi.common.core.domain.entity.SysDept;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 抖音分类对象 loyj_douyin_class
 *
 * @author loyj
 * @date 2021-10-29
 */
@Data
public class DouyinClass
{
    private static final long serialVersionUID = 1L;

    /** 分类主键ID */
    private String id;

    /** 父类ID */
    @Excel(name = "父类ID")
    private String pid;

    /** 抖音分类名称 */
    @Excel(name = "抖音分类名称")
    private String dyClassName;

    /** 排序顺序 */
    @Excel(name = "排序顺序")
    private Integer orderNum;

    /** 子分类 */
    private List<DouyinClass> children = new ArrayList<>();
}
