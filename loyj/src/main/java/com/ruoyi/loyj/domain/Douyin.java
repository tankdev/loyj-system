package com.ruoyi.loyj.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

/**
 * 抖音解析对象 loyj_douyin
 *
 * @author loyj
 * @date 2021-09-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Douyin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 分类外键ID */
    private String classId;

    /** 作者 */
    @Excel(name = "作者")
    private String author;

    /** 抖音分享的URL */
    @Excel(name = "抖音分享的URL")
    @NotBlank(message = "抖音分享的URL不能为空")
    private String shareUrl;

    /** 抖音标题 */
    @Excel(name = "抖音标题")
    private String title;

    /** 无水印URL */
    @Excel(name = "无水印URL")
    private String noWatermarkUrl;

    /** 音乐标题 */
    @Excel(name = "音乐标题")
    private String musicTitle;

    /** 音乐URL */
    @Excel(name = "音乐URL")
    private String musicUrl;

    /** 评论数量 */
    @Excel(name = "评论数量")
    private Long commentCount;

    /** 点赞数量 */
    @Excel(name = "点赞数量")
    private Long  diggCount;

    /** 分享数量 */
    @Excel(name = "分享数量")
    private Long shareCount;

    /** 照片URL */
    @JsonIgnore
    private String photoUrl;

    private List<String> photos;
}
