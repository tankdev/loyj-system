package com.ruoyi.loyj.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.loyj.domain.DouyinPhoto;
import com.ruoyi.loyj.service.IDouyinPhotoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 抖音照片Controller
 *
 * @author loyj
 * @date 2021-10-30
 */
@RestController
@RequestMapping("/spider/photo")
public class DouyinPhotoController extends BaseController
{
    @Autowired
    private IDouyinPhotoService douyinPhotoService;

    /**
     * 查询抖音照片列表
     */
    @PreAuthorize("@ss.hasPermi('loyj:photo:list')")
    @GetMapping("/list")
    public TableDataInfo list(DouyinPhoto douyinPhoto)
    {
        startPage();
        List<DouyinPhoto> list = douyinPhotoService.selectDouyinPhotoList(douyinPhoto);
        return getDataTable(list);
    }

    /**
     * 导出抖音照片列表
     */
    @PreAuthorize("@ss.hasPermi('loyj:photo:export')")
    @Log(title = "抖音照片", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DouyinPhoto douyinPhoto)
    {
        List<DouyinPhoto> list = douyinPhotoService.selectDouyinPhotoList(douyinPhoto);
        ExcelUtil<DouyinPhoto> util = new ExcelUtil<DouyinPhoto>(DouyinPhoto.class);
        return util.exportExcel(list, "抖音照片数据");
    }

    /**
     * 获取抖音照片详细信息
     */
    @PreAuthorize("@ss.hasPermi('loyj:photo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(douyinPhotoService.selectDouyinPhotoById(id));
    }

    /**
     * 新增抖音照片
     */
    @PreAuthorize("@ss.hasPermi('loyj:photo:add')")
    @Log(title = "抖音照片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DouyinPhoto douyinPhoto)
    {
        return toAjax(douyinPhotoService.insertDouyinPhoto(douyinPhoto));
    }

    /**
     * 修改抖音照片
     */
    @PreAuthorize("@ss.hasPermi('loyj:photo:edit')")
    @Log(title = "抖音照片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DouyinPhoto douyinPhoto)
    {
        return toAjax(douyinPhotoService.updateDouyinPhoto(douyinPhoto));
    }

    /**
     * 删除抖音照片
     */
    @PreAuthorize("@ss.hasPermi('loyj:photo:remove')")
    @Log(title = "抖音照片", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(douyinPhotoService.deleteDouyinPhotoByIds(ids));
    }

    /**
     * 通过抖音ID查询抖音照片列表
     */
    @PreAuthorize("@ss.hasPermi('loyj:photo:list')")
    @GetMapping("/list/{id}")
    public AjaxResult listByDouyinId(@PathVariable String id)
    {
        return AjaxResult.success(douyinPhotoService.selectDouyinPhotoByDouyinId(id));
    }
}
