package com.ruoyi.loyj.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.loyj.domain.DouyinClass;
import com.ruoyi.loyj.service.IDouyinClassService;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 抖音分类Controller
 *
 * @author loyj
 * @date 2021-10-29
 */
@RestController
@RequestMapping("/spider/douyinClass")
public class DouyinClassController extends BaseController
{
    @Autowired
    private IDouyinClassService douyinClassService;

    /**
     * 查询抖音分类列表
     */
    @PreAuthorize("@ss.hasPermi('spider:douyinClass:list')")
    @GetMapping("/list")
    public AjaxResult list(DouyinClass douyinClass)
    {
        List<DouyinClass> list = douyinClassService.selectDouyinClassList(douyinClass);
        return AjaxResult.success(list);
    }

    /**
     * 导出抖音分类列表
     */
    @PreAuthorize("@ss.hasPermi('spider:douyinClass:export')")
    @Log(title = "抖音分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DouyinClass douyinClass)
    {
        List<DouyinClass> list = douyinClassService.selectDouyinClassList(douyinClass);
        ExcelUtil<DouyinClass> util = new ExcelUtil<DouyinClass>(DouyinClass.class);
        return util.exportExcel(list, "抖音分类数据");
    }

    /**
     * 获取抖音分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('spider:douyinClass:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(douyinClassService.selectDouyinClassById(id));
    }

    /**
     * 新增抖音分类
     */
    @PreAuthorize("@ss.hasPermi('spider:douyinClass:add')")
    @Log(title = "抖音分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DouyinClass douyinClass)
    {
        return toAjax(douyinClassService.insertDouyinClass(douyinClass));
    }

    /**
     * 修改抖音分类
     */
    @PreAuthorize("@ss.hasPermi('spider:douyinClass:edit')")
    @Log(title = "抖音分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DouyinClass douyinClass)
    {
        return toAjax(douyinClassService.updateDouyinClass(douyinClass));
    }

    /**
     * 删除抖音分类
     */
    @PreAuthorize("@ss.hasPermi('spider:douyinClass:remove')")
    @Log(title = "抖音分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(douyinClassService.deleteDouyinClassByIds(ids));
    }

    @GetMapping("/treeselect")
    public AjaxResult tree(){
        List<DouyinClass> douyinClasses = douyinClassService.selectDouyinClassList(null);
        return AjaxResult.success(douyinClassService.buildDouyinClassTreeSelect(douyinClasses));
    }
}
