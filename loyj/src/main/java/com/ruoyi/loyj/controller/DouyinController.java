package com.ruoyi.loyj.controller;

import java.util.List;

import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.loyj.domain.Douyin;
import com.ruoyi.loyj.service.IDouyinService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 抖音解析Controller
 *
 * @author loyj
 * @date 2021-09-10
 */
@RestController
@RequestMapping("/spider/douyin")
public class DouyinController extends BaseController
{
    @Autowired
    private IDouyinService douyinService;

    /**
     * 查询抖音解析列表
     */
    @PreAuthorize("@ss.hasPermi('spider:douyin:list')")
    @GetMapping("/list")
    public TableDataInfo list(Douyin douyin)
    {
        startPage();
        List<Douyin> list = douyinService.selectDouyinList(douyin);
        return getDataTable(list);
    }

    /**
     * 导出抖音解析列表
     */
    @PreAuthorize("@ss.hasPermi('spider:douyin:export')")
    @Log(title = "抖音解析", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Douyin douyin)
    {
        List<Douyin> list = douyinService.selectDouyinList(douyin);
        ExcelUtil<Douyin> util = new ExcelUtil<Douyin>(Douyin.class);
        return util.exportExcel(list, "抖音解析数据");
    }

    /**
     * 获取抖音解析详细信息
     */
    @PreAuthorize("@ss.hasPermi('spider:douyin:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(douyinService.selectDouyinById(id));
    }

    /**
     * 新增抖音解析
     */
    @PreAuthorize("@ss.hasPermi('spider:douyin:add')")
    @Log(title = "抖音解析", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody Douyin douyin)
    {
        return toAjax(douyinService.insertDouyin(douyin));
    }

    /**
     * 修改抖音解析
     */
    @PreAuthorize("@ss.hasPermi('spider:douyin:edit')")
    @Log(title = "抖音解析", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Douyin douyin)
    {
        douyin.setUpdateBy(getUsername());
        return toAjax(douyinService.updateDouyin(douyin));
    }

    /**
     * 删除抖音解析
     */
    @PreAuthorize("@ss.hasPermi('spider:douyin:remove')")
    @Log(title = "抖音解析", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(douyinService.deleteDouyinByIds(ids));
    }
}
