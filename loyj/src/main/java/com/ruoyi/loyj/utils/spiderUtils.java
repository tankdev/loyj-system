package com.ruoyi.loyj.utils;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 爬虫工具类
 *
 * @author loyj
 * @create 2021年09月22日 11:21
 */
public class spiderUtils {

    /**
     * 请求链接URL.
     * @param url 被链接URL
     * @return 响应
     */
    public static Connection.Response requireUrl(String url){
        Connection.Response execute = null;
        try {
            execute = Jsoup.connect(url)
                    .userAgent("Dalvik/2.1.0 (Linux; U; Android 11)")
                    .ignoreContentType(true)
                    .followRedirects(true)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return execute;
    }


    /**
     * 正则提取字符串
     * @param str   被提取的字符串
     * @param regex 提取规则
     * @return 提取的字符
     */
    public static String regexMatch(String str, String regex){
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        String subStr = null;
        while(m.find()) {
            subStr = m.group(1).trim();
        }
        return subStr;
    }


    /**
     * 时间戳转时间.
     *
     * @param s 时间戳
     * @return 时间字符串
     */
    public static Date stampToDate(Long s){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(s);
        try
        {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(simpleDateFormat.format(date));
        }
        catch (ParseException e)
        {
            throw new RuntimeException(e);
        }
    }

}
