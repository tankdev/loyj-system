package com.ruoyi.loyj.mapper;

import java.util.List;
import com.ruoyi.loyj.domain.DouyinPhoto;
import org.apache.ibatis.annotations.Mapper;

/**
 * 抖音照片Mapper接口
 *
 * @author loyj
 * @date 2021-10-30
 */
@Mapper
public interface DouyinPhotoMapper
{
    /**
     * 查询抖音照片
     *
     * @param id 抖音照片主键
     * @return 抖音照片
     */
    public DouyinPhoto selectDouyinPhotoById(String id);

    /**
     * 查询抖音照片列表
     *
     * @param douyinPhoto 抖音照片
     * @return 抖音照片集合
     */
    public List<DouyinPhoto> selectDouyinPhotoList(DouyinPhoto douyinPhoto);

    /**
     * 新增抖音照片
     *
     * @param douyinPhoto 抖音照片
     * @return 结果
     */
    public int insertDouyinPhoto(DouyinPhoto douyinPhoto);

    /**
     * 修改抖音照片
     *
     * @param douyinPhoto 抖音照片
     * @return 结果
     */
    public int updateDouyinPhoto(DouyinPhoto douyinPhoto);

    /**
     * 删除抖音照片
     *
     * @param id 抖音照片主键
     * @return 结果
     */
    public int deleteDouyinPhotoById(String id);

    /**
     * 批量删除抖音照片
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDouyinPhotoByIds(String[] ids);

    /**
     * 批量新增抖音照片
     *
     * @param douyinPhotos 抖音照片列表
     * @return 结果
     */
    int batchInsertDouyinPhoto(List<DouyinPhoto> douyinPhotos);

    /**
     * 通过抖音ID查询抖音照片
     *
     * @param id 抖音ID
     * @return 抖音照片
     */
    List<String> selectDouyinPhotoByDouyinId(String id);
}
