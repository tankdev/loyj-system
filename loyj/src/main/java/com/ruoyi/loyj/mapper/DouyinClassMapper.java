package com.ruoyi.loyj.mapper;

import java.util.List;
import com.ruoyi.loyj.domain.DouyinClass;
import org.apache.ibatis.annotations.Mapper;

/**
 * 抖音分类Mapper接口
 *
 * @author loyj
 * @date 2021-10-29
 */
@Mapper
public interface DouyinClassMapper
{
    /**
     * 查询抖音分类
     *
     * @param id 抖音分类主键
     * @return 抖音分类
     */
    public DouyinClass selectDouyinClassById(String id);

    /**
     * 查询抖音分类列表
     *
     * @param douyinClass 抖音分类
     * @return 抖音分类集合
     */
    public List<DouyinClass> selectDouyinClassList(DouyinClass douyinClass);

    /**
     * 新增抖音分类
     *
     * @param douyinClass 抖音分类
     * @return 结果
     */
    public int insertDouyinClass(DouyinClass douyinClass);

    /**
     * 修改抖音分类
     *
     * @param douyinClass 抖音分类
     * @return 结果
     */
    public int updateDouyinClass(DouyinClass douyinClass);

    /**
     * 删除抖音分类
     *
     * @param id 抖音分类主键
     * @return 结果
     */
    public int deleteDouyinClassById(String id);

    /**
     * 批量删除抖音分类
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDouyinClassByIds(String[] ids);
}
