package com.ruoyi.loyj.mapper;

import java.util.List;
import com.ruoyi.loyj.domain.Douyin;
import org.apache.ibatis.annotations.Mapper;

/**
 * 抖音解析Mapper接口
 * 
 * @author loyj
 * @date 2021-09-10
 */
@Mapper
public interface DouyinMapper 
{
    /**
     * 查询抖音解析
     * 
     * @param id 抖音解析主键
     * @return 抖音解析
     */
    public Douyin selectDouyinById(String id);

    /**
     * 查询抖音解析列表
     * 
     * @param douyin 抖音解析
     * @return 抖音解析集合
     */
    public List<Douyin> selectDouyinList(Douyin douyin);

    /**
     * 新增抖音解析
     * 
     * @param douyin 抖音解析
     * @return 结果
     */
    public int insertDouyin(Douyin douyin);

    /**
     * 修改抖音解析
     * 
     * @param douyin 抖音解析
     * @return 结果
     */
    public int updateDouyin(Douyin douyin);

    /**
     * 删除抖音解析
     * 
     * @param id 抖音解析主键
     * @return 结果
     */
    public int deleteDouyinById(String id);

    /**
     * 批量删除抖音解析
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDouyinByIds(String[] ids);
}
