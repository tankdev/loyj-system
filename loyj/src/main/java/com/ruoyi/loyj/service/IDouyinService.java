package com.ruoyi.loyj.service;

import java.util.List;
import com.ruoyi.loyj.domain.Douyin;

/**
 * 抖音解析Service接口
 *
 * @author loyj
 * @date 2021-09-10
 */
public interface IDouyinService
{
    /**
     * 查询抖音解析
     *
     * @param id 抖音解析主键
     * @return 抖音解析
     */
    public Douyin selectDouyinById(String id);

    /**
     * 查询抖音解析列表
     *
     * @param douyin 抖音解析
     * @return 抖音解析集合
     */
    public List<Douyin> selectDouyinList(Douyin douyin);

    /**
     * 新增抖音解析
     *
     * @param douyin 抖音解析
     * @return 结果
     */
    public int insertDouyin(Douyin douyin);

    /**
     * 修改抖音解析
     *
     * @param douyin 抖音解析
     * @return 结果
     */
    public int updateDouyin(Douyin douyin);

    /**
     * 批量删除抖音解析
     *
     * @param ids 需要删除的抖音解析主键集合
     * @return 结果
     */
    public int deleteDouyinByIds(String[] ids);

    /**
     * 删除抖音解析信息
     *
     * @param id 抖音解析主键
     * @return 结果
     */
    public int deleteDouyinById(String id);
}
