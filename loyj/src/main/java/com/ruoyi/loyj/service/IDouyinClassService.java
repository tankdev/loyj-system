package com.ruoyi.loyj.service;

import java.util.List;
import com.ruoyi.loyj.domain.DouyinClass;
import com.ruoyi.loyj.domain.BuildTree;

/**
 * 抖音分类Service接口
 *
 * @author loyj
 * @date 2021-10-29
 */
public interface IDouyinClassService
{
    /**
     * 查询抖音分类
     *
     * @param id 抖音分类主键
     * @return 抖音分类
     */
    public DouyinClass selectDouyinClassById(String id);

    /**
     * 查询抖音分类列表
     *
     * @param douyinClass 抖音分类
     * @return 抖音分类集合
     */
    public List<DouyinClass> selectDouyinClassList(DouyinClass douyinClass);

    /**
     * 新增抖音分类
     *
     * @param douyinClass 抖音分类
     * @return 结果
     */
    public int insertDouyinClass(DouyinClass douyinClass);

    /**
     * 修改抖音分类
     *
     * @param douyinClass 抖音分类
     * @return 结果
     */
    public int updateDouyinClass(DouyinClass douyinClass);

    /**
     * 批量删除抖音分类
     *
     * @param ids 需要删除的抖音分类主键集合
     * @return 结果
     */
    public int deleteDouyinClassByIds(String[] ids);

    /**
     * 删除抖音分类信息
     *
     * @param id 抖音分类主键
     * @return 结果
     */
    public int deleteDouyinClassById(String id);

    /**
     * 构建前端所需要树结构
     *
     * @param dyClass 分类列表
     * @return 树结构列表
     */
    public List<DouyinClass> buildDouyinClassTree(List<DouyinClass> dyClass);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param dyc 分类列表
     * @return 下拉树结构列表
     */
    public List<BuildTree> buildDouyinClassTreeSelect(List<DouyinClass> dyc);

    /**
     * 获取抖音分类所有的字孙ID
     *
     * @param dyClass 分类列表
     * @return 树结构列表
     */
    public List<String> getDouyinClassChildren(List<DouyinClass> dyClass);
}
