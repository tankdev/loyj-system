package com.ruoyi.loyj.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.loyj.domain.BuildTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.loyj.mapper.DouyinClassMapper;
import com.ruoyi.loyj.domain.DouyinClass;
import com.ruoyi.loyj.service.IDouyinClassService;

/**
 * 抖音分类Service业务层处理
 *
 * @author loyj
 * @date 2021-10-29
 */
@Service
public class DouyinClassServiceImpl implements IDouyinClassService
{
    @Autowired
    private DouyinClassMapper douyinClassMapper;

    /**
     * 查询抖音分类
     *
     * @param id 抖音分类主键
     * @return 抖音分类
     */
    @Override
    public DouyinClass selectDouyinClassById(String id)
    {
        return douyinClassMapper.selectDouyinClassById(id);
    }

    /**
     * 查询抖音分类列表
     *
     * @param douyinClass 抖音分类
     * @return 抖音分类
     */
    @Override
    public List<DouyinClass> selectDouyinClassList(DouyinClass douyinClass)
    {
        return douyinClassMapper.selectDouyinClassList(douyinClass);
    }

    /**
     * 新增抖音分类
     *
     * @param douyinClass 抖音分类
     * @return 结果
     */
    @Override
    public int insertDouyinClass(DouyinClass douyinClass)
    {
        douyinClass.setId(IdUtils.fastUUID());
        return douyinClassMapper.insertDouyinClass(douyinClass);
    }

    /**
     * 修改抖音分类
     *
     * @param douyinClass 抖音分类
     * @return 结果
     */
    @Override
    public int updateDouyinClass(DouyinClass douyinClass)
    {
        return douyinClassMapper.updateDouyinClass(douyinClass);
    }

    /**
     * 批量删除抖音分类
     *
     * @param ids 需要删除的抖音分类主键
     * @return 结果
     */
    @Override
    public int deleteDouyinClassByIds(String[] ids)
    {
        return douyinClassMapper.deleteDouyinClassByIds(ids);
    }

    /**
     * 删除抖音分类信息
     *
     * @param id 抖音分类主键
     * @return 结果
     */
    @Override
    public int deleteDouyinClassById(String id)
    {
        return douyinClassMapper.deleteDouyinClassById(id);
    }

    /**
     * 构建前端所需要树结构
     *
     * @param dyClass 分类列表
     * @return 树结构列表
     */
    @Override
    public List<DouyinClass> buildDouyinClassTree(List<DouyinClass> dyClass)
    {
        List<DouyinClass> returnList = new ArrayList<>();
        List<String> tempList = new ArrayList<>();
        for (DouyinClass d : dyClass)
        {
            tempList.add(d.getId());
        }
        for (Iterator<DouyinClass> iterator = dyClass.iterator(); iterator.hasNext();)
        {
            DouyinClass dyclass = (DouyinClass) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dyclass.getPid()))
            {
                recursionFn(dyClass, dyclass);
                returnList.add(dyclass);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = dyClass;
        }
        return returnList;
    }

    /**
     * 构建前端所需要下拉树结构
     *
     * @param dyc 分类列表
     * @return 下拉树结构列表
     */
    @Override
    public List<BuildTree> buildDouyinClassTreeSelect(List<DouyinClass> dyc)
    {
        List<DouyinClass> dycTrees = buildDouyinClassTree(dyc);
        return dycTrees.stream().map(BuildTree::new).collect(Collectors.toList());
    }


    /**
     * 获取抖音分类所有的字孙ID
     *
     * @param dyClass 分类列表
     * @return 树结构列表
     */
    @Override
    public List<String> getDouyinClassChildren(List<DouyinClass> dyClass)
    {
        List<String> returnList = new ArrayList<>();
        List<String> tempList = new ArrayList<>();
        for (DouyinClass d : dyClass)
        {
            tempList.add(d.getId());
        }
        for (Iterator<DouyinClass> iterator = dyClass.iterator(); iterator.hasNext();)
        {
            DouyinClass dyclass = (DouyinClass) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dyclass.getPid()))
            {
                recursionFn(dyClass, dyclass, returnList);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = dyClass.stream().map(DouyinClass::getId).collect(Collectors.toList());
        }
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<DouyinClass> list, DouyinClass t)
    {
        // 得到子节点列表
        List<DouyinClass> childList = getChildList(list, t);
        t.setChildren(childList);
        for (DouyinClass tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<DouyinClass> list, DouyinClass t, List<String> ids)
    {
        // 得到子节点列表
        List<DouyinClass> childList = getChildList(list, t);
        ids.addAll(childList.stream().map(DouyinClass::getId).collect(Collectors.toList()));
        for (DouyinClass tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild, ids);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<DouyinClass> getChildList(List<DouyinClass> list, DouyinClass t)
    {
        List<DouyinClass> tlist = new ArrayList<>();
        Iterator<DouyinClass> it = list.iterator();
        while (it.hasNext())
        {
            DouyinClass n = it.next();
            if (StringUtils.isNotNull(n.getPid()) && n.getPid().equals(t.getId()))
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<DouyinClass> list, DouyinClass t)
    {
        return getChildList(list, t).size() > 0;
    }
}
