package com.ruoyi.loyj.service;

import java.util.List;
import com.ruoyi.loyj.domain.DouyinPhoto;
import org.springframework.stereotype.Service;

/**
 * 抖音照片Service接口
 *
 * @author loyj
 * @date 2021-10-30
 */
public interface IDouyinPhotoService
{
    /**
     * 查询抖音照片
     *
     * @param id 抖音照片主键
     * @return 抖音照片
     */
    public DouyinPhoto selectDouyinPhotoById(String id);

    /**
     * 查询抖音照片列表
     *
     * @param douyinPhoto 抖音照片
     * @return 抖音照片集合
     */
    public List<DouyinPhoto> selectDouyinPhotoList(DouyinPhoto douyinPhoto);

    /**
     * 新增抖音照片
     *
     * @param douyinPhoto 抖音照片
     * @return 结果
     */
    public int insertDouyinPhoto(DouyinPhoto douyinPhoto);

    /**
     * 修改抖音照片
     *
     * @param douyinPhoto 抖音照片
     * @return 结果
     */
    public int updateDouyinPhoto(DouyinPhoto douyinPhoto);

    /**
     * 批量删除抖音照片
     *
     * @param ids 需要删除的抖音照片主键集合
     * @return 结果
     */
    public int deleteDouyinPhotoByIds(String[] ids);

    /**
     * 删除抖音照片信息
     *
     * @param id 抖音照片主键
     * @return 结果
     */
    public int deleteDouyinPhotoById(String id);

    /**
     * 通过抖音ID查询抖音照片
     *
     * @param id 抖音ID
     * @return 抖音照片
     */
    List<String> selectDouyinPhotoByDouyinId(String id);
}
