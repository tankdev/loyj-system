package com.ruoyi.loyj.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.loyj.mapper.DouyinPhotoMapper;
import com.ruoyi.loyj.domain.DouyinPhoto;
import com.ruoyi.loyj.service.IDouyinPhotoService;

/**
 * 抖音照片Service业务层处理
 *
 * @author loyj
 * @date 2021-10-30
 */
@Service
public class DouyinPhotoServiceImpl implements IDouyinPhotoService
{
    @Autowired
    private DouyinPhotoMapper douyinPhotoMapper;

    /**
     * 查询抖音照片
     *
     * @param id 抖音照片主键
     * @return 抖音照片
     */
    @Override
    public DouyinPhoto selectDouyinPhotoById(String id)
    {

        return douyinPhotoMapper.selectDouyinPhotoById(id);
    }

    /**
     * 查询抖音照片列表
     *
     * @param douyinPhoto 抖音照片
     * @return 抖音照片
     */
    @Override
    public List<DouyinPhoto> selectDouyinPhotoList(DouyinPhoto douyinPhoto)
    {
        return douyinPhotoMapper.selectDouyinPhotoList(douyinPhoto);
    }

    /**
     * 新增抖音照片
     *
     * @param douyinPhoto 抖音照片
     * @return 结果
     */
    @Override
    public int insertDouyinPhoto(DouyinPhoto douyinPhoto)
    {
        return douyinPhotoMapper.insertDouyinPhoto(douyinPhoto);
    }

    /**
     * 修改抖音照片
     *
     * @param douyinPhoto 抖音照片
     * @return 结果
     */
    @Override
    public int updateDouyinPhoto(DouyinPhoto douyinPhoto)
    {
        return douyinPhotoMapper.updateDouyinPhoto(douyinPhoto);
    }

    /**
     * 批量删除抖音照片
     *
     * @param ids 需要删除的抖音照片主键
     * @return 结果
     */
    @Override
    public int deleteDouyinPhotoByIds(String[] ids)
    {
        return douyinPhotoMapper.deleteDouyinPhotoByIds(ids);
    }

    /**
     * 删除抖音照片信息
     *
     * @param id 抖音照片主键
     * @return 结果
     */
    @Override
    public int deleteDouyinPhotoById(String id)
    {
        return douyinPhotoMapper.deleteDouyinPhotoById(id);
    }


    /**
     * 通过抖音ID查询抖音照片
     *
     * @param id 抖音ID
     * @return 抖音照片
     */
    @Override
    public List<String> selectDouyinPhotoByDouyinId(String id) {
        return douyinPhotoMapper.selectDouyinPhotoByDouyinId(id);
    }
}
