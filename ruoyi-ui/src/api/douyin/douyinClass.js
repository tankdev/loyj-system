import request from '@/utils/request'

// 查询抖音分类列表
export function listDouyinClass(query) {
  return request({
    url: '/spider/douyinClass/list',
    method: 'get',
    params: query
  })
}

// 查询抖音分类详细
export function getDouyinClass(id) {
  return request({
    url: '/spider/douyinClass/' + id,
    method: 'get'
  })
}

// 新增抖音分类
export function addDouyinClass(data) {
  return request({
    url: '/spider/douyinClass',
    method: 'post',
    data: data
  })
}

// 修改抖音分类
export function updateDouyinClass(data) {
  return request({
    url: '/spider/douyinClass',
    method: 'put',
    data: data
  })
}

// 删除抖音分类
export function delDouyinClass(id) {
  return request({
    url: '/spider/douyinClass/' + id,
    method: 'delete'
  })
}

// 导出抖音分类
export function exportDouyinClass(query) {
  return request({
    url: '/spider/douyinClass/export',
    method: 'get',
    params: query
  })
}

// 获取抖音分类树
export function treeselect() {
  return request({
    url: '/spider/douyinClass/treeselect',
    method: 'get'
  })
}