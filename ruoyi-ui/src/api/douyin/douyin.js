import request from '@/utils/request'

// 查询抖音解析列表
export function listDouyin(query) {
  return request({
    url: '/spider/douyin/list',
    method: 'get',
    params: query
  })
}

// 查询抖音解析详细
export function getDouyin(id) {
  return request({
    url: '/spider/douyin/' + id,
    method: 'get'
  })
}

// 新增抖音解析
export function addDouyin(data) {
  return request({
    url: '/spider/douyin',
    method: 'post',
    data: data
  })
}

// 修改抖音解析
export function updateDouyin(data) {
  return request({
    url: '/spider/douyin',
    method: 'put',
    data: data
  })
}

// 删除抖音解析
export function delDouyin(id) {
  return request({
    url: '/spider/douyin/' + id,
    method: 'delete'
  })
}

// 导出抖音解析
export function exportDouyin(query) {
  return request({
    url: '/spider/douyin/export',
    method: 'get',
    params: query
  })
}