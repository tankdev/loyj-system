import request from '@/utils/request'

// 根据抖音ID询抖音解析列表
export function getDouyinPhotoListByDouyinId(id) {
  return request({
    url: '/spider/photo/list/' + id,
    method: 'get'
  })
}
