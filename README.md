## 平台简介
没事儿写点啥，就搞了个这个东西！

* 框架使用的是[RuoYi-Vue](http://doc.ruoyi.vip)的前后端分离版本 
* 爬虫框架使用的是Jsoup
* 添加App端，[点击下载](https://ly3810.lanzouo.com/i64Aax2yedg)

## 功能

* 抖音去水印：直接复粘贴从抖音复制的链接即可，可解析该抖音的音频、去水印视频、图片，无视禁用下载，分类管理
* APP可以看点图，抖音解析视频和图片

## 部署
- 将项目导入idea
- 后端：导入sql文件，修改yaml里面的数据库账号密码，运行ruoyi-admin里面的主方法
- 前端：工程文件是ruoyi-ui, 可在VScode里面打开，并执行cnpm i命令，安装完所有依赖后执行npm run dev命令
- APP前台： 将FirstApp文件在HBuilderX内打开，修改common文件下的requirst.js文件的第一行，将loyj修改为自己设置的（看下一条），后台启动后再启动前台app，需要数据线连接手机调试，H5调试有的方法会报错，调试完成可以发行云打包。
- APP后台： 在tools文件下的app后台服务文件夹中，编辑startDing.bat文件，将loyj改为自己要改的，然后保存启动，在启动startService.bat.
## 账号密码

- web端：admin/admin123
- APP端：无

## 图示
![输入图片说明](https://images.gitee.com/uploads/images/2021/1030/113759_147f1a48_8224643.png "屏幕截图.png")

![输入图片说明](doc/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20211201093057.jpg)