/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : loyj

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 11/11/2021 14:49:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
                              `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
                              `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表名称',
                              `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表描述',
                              `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
                              `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
                              `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '实体类名称',
                              `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
                              `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成包路径',
                              `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成模块名',
                              `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成业务名',
                              `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能名',
                              `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能作者',
                              `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
                              `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
                              `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '其它生成选项',
                              `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                              `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                              `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                              `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                              `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                              PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'loyj_douyin', '抖音解析', NULL, NULL, 'Douyin', 'crud', 'com.ruoyi.loyj', 'spider', 'douyin', '抖音解析', 'loyj', '0', '/', '{\"parentMenuId\":\"2001\"}', 'admin', '2021-09-10 23:32:15', '', '2021-09-22 15:22:11', NULL);
INSERT INTO `gen_table` VALUES (2, 'loyj_douyin_photo', '抖音照片表', NULL, NULL, 'DouyinPhoto', 'crud', 'com.ruoyi.loyj', 'loyj', 'photo', '抖音照片', 'loyj', '0', '/', '{}', 'admin', '2021-10-22 23:27:16', '', '2021-10-30 11:06:02', NULL);
INSERT INTO `gen_table` VALUES (3, 'loyj_douyin_class', '抖音分类表', '', '', 'DouyinClass', 'tree', 'com.ruoyi.loyj', 'spider', 'douyinClass', '抖音分类', 'loyj', '0', '/', '{\"treeCode\":\"id\",\"treeName\":\"dy_class_name\",\"treeParentCode\":\"pid\"}', 'admin', '2021-10-29 19:10:40', '', '2021-10-29 19:13:31', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
                                     `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
                                     `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '归属表编号',
                                     `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列名称',
                                     `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列描述',
                                     `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列类型',
                                     `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
                                     `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
                                     `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
                                     `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
                                     `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
                                     `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
                                     `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
                                     `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
                                     `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
                                     `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
                                     `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
                                     `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
                                     `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
                                     `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                                     `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                     `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                                     `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                     PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'id', '主键ID', 'varchar(36)', 'String', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2021-09-10 23:32:15', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (2, '1', 'share_url', '抖音分享的URL', 'varchar(50)', 'String', 'shareUrl', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'input', '', 2, 'admin', '2021-09-10 23:32:15', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (3, '1', 'title', '抖音标题', 'varchar(300)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2021-09-10 23:32:15', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (4, '1', 'no_watermark_url', '无水印URL', 'varchar(300)', 'String', 'noWatermarkUrl', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 4, 'admin', '2021-09-10 23:32:15', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (5, '1', 'create_by', '创建人', 'varchar(50)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 5, 'admin', '2021-09-10 23:32:15', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (6, '1', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2021-09-10 23:32:15', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (7, '1', 'update_By', '更新人', 'varchar(50)', 'String', 'updateBy', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 7, 'admin', '2021-09-10 23:32:15', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (8, '1', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2021-09-10 23:32:15', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (9, '1', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 9, 'admin', '2021-09-10 23:32:15', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (10, '1', 'author', '作者', 'varchar(50)', 'String', 'author', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, '', '2021-09-22 15:17:05', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (17, '1', 'release_time', '发布时间', 'datetime', 'Date', 'releaseTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 5, '', '2021-09-22 15:21:00', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (18, '1', 'music_title', '音乐标题', 'varchar(50)', 'String', 'musicTitle', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 6, '', '2021-09-22 15:21:00', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (19, '1', 'music_url', '音乐URL', 'varchar(300)', 'String', 'musicUrl', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 7, '', '2021-09-22 15:21:00', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (20, '1', 'comment_count', '评论数量', 'bigint(20)', 'Long', 'commentCount', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 9, '', '2021-09-22 15:21:00', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (21, '1', 'digg_count', '点赞数量', 'bigint(20)', 'Long', 'diggCount', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 10, '', '2021-09-22 15:21:00', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (22, '1', 'share_count', '分享数量', 'bigint(20)', 'Long', 'shareCount', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 11, '', '2021-09-22 15:21:00', '', '2021-09-22 15:22:11');
INSERT INTO `gen_table_column` VALUES (23, '2', 'id', '主键ID', 'varchar(32)', 'String', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2021-10-22 23:27:16', '', '2021-10-30 11:06:02');
INSERT INTO `gen_table_column` VALUES (26, '3', 'id', '分类主键ID', 'varchar(36)', 'String', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2021-10-29 19:10:40', '', '2021-10-29 19:13:31');
INSERT INTO `gen_table_column` VALUES (27, '3', 'pid', '父类ID', 'varchar(36)', 'String', 'pid', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 2, 'admin', '2021-10-29 19:10:40', '', '2021-10-29 19:13:31');
INSERT INTO `gen_table_column` VALUES (28, '3', 'dy_class_name', '抖音分类名称', 'varchar(255)', 'String', 'dyClassName', '0', '0', '1', '1', '1', '1', NULL, 'LIKE', 'input', '', 3, 'admin', '2021-10-29 19:10:40', '', '2021-10-29 19:13:31');
INSERT INTO `gen_table_column` VALUES (29, '3', 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 4, 'admin', '2021-10-29 19:10:40', '', '2021-10-29 19:13:31');
INSERT INTO `gen_table_column` VALUES (30, '2', 'douyin_id', '抖音外键ID', 'varchar(36)', 'String', 'douyinId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, '', '2021-10-30 11:05:39', '', '2021-10-30 11:06:02');
INSERT INTO `gen_table_column` VALUES (31, '2', 'photo_url', '照片链接', 'varchar(255)', 'String', 'photoUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, '', '2021-10-30 11:05:39', '', '2021-10-30 11:06:02');

-- ----------------------------
-- Table structure for loyj_douyin
-- ----------------------------
DROP TABLE IF EXISTS `loyj_douyin`;
CREATE TABLE `loyj_douyin`  (
                                `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键ID',
                                `class_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '抖音分类外键ID',
                                `author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '作者',
                                `share_url` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '抖音分享的URL',
                                `title` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '抖音标题',
                                `music_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '音乐标题',
                                `music_url` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '音乐URL',
                                `no_watermark_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '无水印URL',
                                `comment_count` bigint(20) NULL DEFAULT NULL COMMENT '评论数量',
                                `digg_count` bigint(20) NULL DEFAULT NULL COMMENT '点赞数量',
                                `share_count` bigint(20) NULL DEFAULT NULL COMMENT '分享数量',
                                `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
                                `create_time` datetime(0) NOT NULL COMMENT '创建时间',
                                `update_By` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
                                `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of loyj_douyin
-- ----------------------------
INSERT INTO `loyj_douyin` VALUES ('0b8707862b36481895aad99d2cee3edb', '6a62e9d2-433c-4201-8199-590431086924', '一只小伊甜', 'https://v.douyin.com/RLp53ab/', '🔥二选一了 家人们@抖音小助手 #闺蜜写真 #模特 #水手服', '@izayoiRui创作的原声', 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/6945340237224659720.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0d00fg10000c5r41ibc77u1fs9smcvg&ratio=720p&line=0', 7817, 141763, 32987, 'admin', '2021-10-27 19:28:27', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('12e7367f359043829577aa44d89826b4', '6a62e9d2-433c-4201-8199-590431086924', '仓大佬', 'https://v.douyin.com/RLsCNw6/', '“你听着……你永远都是我的，逃不掉的。”#国风合伙人 #异域风情 #异域国风美人天花板', '@路一.创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6983246060844059423.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c5r7bpbc77uej7lt62eg&ratio=720p&line=0', 2938, 95322, 11211, 'admin', '2021-10-27 19:51:19', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('1625cac5c9f14c15b7f65d7dc5d2a25a', '6a62e9d2-433c-4201-8199-590431086924', '美女搜集社', 'https://v.douyin.com/RLpvy9M/', '如果一千个W和她只能选一个，你会怎么选？#拉拉队美女  #歌曲问情 ', '@阿悠悠创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7019615910072027917.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c5pe1crc77u8275d1bjg&ratio=720p&line=0', 24, 1127, 52, 'admin', '2021-10-27 19:35:01', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('1beed64d849c4863a7922ef47c23b32b', '97aa1525-ec98-4031-aa77-c0e4735a4fff', '尚好', 'https://v.douyin.com/RAyWfQX/', '出来单挑啊#出来单挑', '@幸运的老杨创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7018028634896190245.mp3', 'https://v26-cold.douyinvod.com/c372f3c9a5d1a3f1057c27fe611848a6/61867d58/video/tos/cn/tos-cn-ve-15-alinc2/73840943da1143b5a38e9d3fd807c88e/?a=1128&br=356&bt=356&cd=0%7C0%7C0&ch=96&cr=0&cs=0&cv=1&dr=0&ds=6&er=&ft=r5zpj77JWMFBMXcI3mo~_arK_&l=202111062004150102121450383725CDDF&lr=all&mime_type=video_mp4&net=0&pl=0&qs=0&rc=Mzx5dGU6Znc0OTMzNGkzM0ApOzo6ODY2Zzs2N2k6N2c7PGcpaHV2fWVuZDFwekBoc2UtcjRvay5gLS1kLWFzczUvMC02MjIyYzAtYl9jYGI6Y29zYlxmK2BtYmJeYA%3D%3D&vl=&vr=', 1057, 5751, 6465, 'admin', '2021-11-06 20:04:15', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('1e664485c10749ccb00a5047d7daa171', '97aa1525-ec98-4031-aa77-c0e4735a4fff', '𝕸𝖗.𝕿𝖆𝖓.𝖄𝖆𝖓𝖌', 'https://v.douyin.com/RyvDFUS/', '#TuPac   .#HipHop   .#WestCoast   .#OldSchool', '@𝕸𝖗.𝕿𝖆𝖓.𝖄𝖆𝖓𝖌创作的原声', 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7022120261029841678.mp3', 'https://v26-cold.douyinvod.com/3006699bd4dcf06ca5b7434d00616240/618a4c41/video/tos/cn/tos-cn-ve-15-alinc2/474d9bdf9fd644d2b5c1fe993defcd2c/?a=1128&br=958&bt=958&cd=0%7C0%7C0&ch=96&cr=0&cs=0&cv=1&dr=0&ds=6&er=&ft=5q8kcGmmnPNT2Fh7HhWwkXAGfdH.CgkQGBZc&l=202111091723430101940510963205D1A7&lr=all&mime_type=video_mp4&net=0&pl=0&qs=0&rc=am1tcjo6ZmRzODMzNGkzM0ApNDczODU8PDw8N2k0NTZnOGcpaHV2fWVuZDFwekBhZ3JhcjRvNm1gLS1kLWFzcy0yNmNeYWE0YTUtMy0vMDE6Y29zYlxmK2BtYmJeYA%3D%3D&vl=&vr=', 486, 23110, 3007, 'admin', '2021-11-09 17:23:44', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('22ded62f0139492ba5cf07d6e56897b6', 'dba39222-deb8-43c4-845c-d84efc72dc26', '杨二狗搞笑段子', 'https://v.douyin.com/RYwsEyn/', '我以为……这也太尴尬了#看一遍笑一遍 #一定要看到最后 #艾特你想艾特的人', '@🌟範王明崔 VAX创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6996556692544555813.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c5ub19bc77u5iqrvrjfg&ratio=720p&line=0', 8593, 32172, 38674, 'admin', '2021-10-30 15:28:37', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('395d87a9d8084f76bd129c6cd1d98019', '97aa1525-ec98-4031-aa77-c0e4735a4fff', '潮图谷【渣弟】', 'https://v.douyin.com/RUBnCQY/', '天凉了，记得多盖点土！#不回消息表情包 #表情包 #抖音小助手 ', '@潮图谷【渣弟】创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7027657709415631629.mp3', 'https://v6.douyinvod.com/39a7fd652afdb57aa19ae924ccce051b/61888545/video/tos/cn/tos-cn-ve-15-alinc2/a86aa029a57b439b90a68e2f8aa3e73a/?a=1128&br=515&bt=515&cd=0%7C0%7C0&ch=96&cr=0&cs=0&cv=1&dr=0&ds=6&er=&ft=OR_LrKZZI0271eTzWTh94ictRsWd.uc6t68&l=202111080902360102040240302E068667&lr=all&mime_type=video_mp4&net=0&pl=0&qs=0&rc=anR2a2Q6ZjxuOTMzNGkzM0ApaDg3OmU6NTs0Nzk1OTU1NmcpaHV2fWVuZDFwekBtNTMxcjRnLzBgLS1kLS9zc2JjXy1eLjYwMGNeLjNgMDM6Y29zYlxmK2BtYmJeYA%3D%3D&vl=&vr=', 6043, 106251, 77248, 'admin', '2021-11-08 09:02:36', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('4a9b664f3ec348708ede06e3b1ae1e47', '6a62e9d2-433c-4201-8199-590431086924', '国际超模大赛', 'https://v.douyin.com/RLguFSv/', '这么多比基尼美女站在我面前，我到底要怎么选呢？😛#比基尼 #大长腿 #女神 #泳装美女 #美女 #国际超模大赛 #花絮 #泳池派对 #身材 ', '@国际超模大赛创作的原声', 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7012906145551518478.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c59dgq3c77u3aqnngjug&ratio=720p&line=0', 69899, 279636, 205874, 'admin', '2021-10-27 19:15:08', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('4aabcc8843864a008836bcf053d92416', '6a62e9d2-433c-4201-8199-590431086924', '天降之雯💫（带牙套中）', 'https://v.douyin.com/RLscJuF/', '#withoutme摇摆舞 绑头发版本怎么样可以吗', '@UoiioU创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7009119494387813150.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0d00fg10000c5m1qnrc77u0au9ibbm0&ratio=720p&line=0', 4161, 167517, 19037, 'admin', '2021-10-27 19:49:13', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('4b50de74942548b29b90ede927c1042e', '6a62e9d2-433c-4201-8199-590431086924', '是你静哥❤️', 'https://v.douyin.com/RLt5NEf/', '不给腿买个保险都不敢出来跳舞#no9应援舞挑战  #no9 #no9舞蹈 ', '@Wink眨宝momo创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6909109288132741902.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c5mjddbc77u7o4q3vj90&ratio=720p&line=0', 3988, 125497, 19334, 'admin', '2021-10-27 20:20:50', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('4bf75150b5b949d08196fa10ca6f83ef', '6a62e9d2-433c-4201-8199-590431086924', '新华网', 'https://v.douyin.com/RFE5nW5/', '国社小姐姐改词翻唱《后妈茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”', '@新华网创作的原声', '', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c5tjsn3c77ueachdm8u0&ratio=720p&line=0', 85036, 2401065, 221846, 'admin', '2021-10-29 18:47:56', 'admin', '2021-10-29 23:39:13', NULL);
INSERT INTO `loyj_douyin` VALUES ('7d0b3e6813ba4a14a70672020d5ac406', 'e0c8a90d-c922-4416-8c3c-37eb844d983a', '天津地铁迷', 'https://v.douyin.com/RYbELGb/', '#狗头 ', '@7ex创作的原声', 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7002996187171261198.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c5tufhrc77u69mufpgh0&ratio=720p&line=0', 65260, 22258, 1256, 'admin', '2021-10-30 10:36:00', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('9eef0c8b8b074d978afaadecaee55eab', '7c37cf38-30d6-4076-9a6d-53688de25e31', '依旧热爱🖤ktm', 'https://v.douyin.com/RAf13Lv/', '“不需要你看上我，我根本看不见你”', '@CJ.创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7023752312271227684.mp3', 'https://v29-cold.douyinvod.com/cdb8600f7900b24ab1df8a14afe88ec4/61867d9e/video/tos/cn/tos-cn-ve-15-alinc2/7e38fc0006ea48a38afd368b46c9f354/?a=1128&br=528&bt=528&cd=0%7C0%7C0&ch=96&cr=0&cs=0&cv=1&dr=0&ds=6&er=&ft=r5zpj77JWMFBMXcI3moLAarK_&l=202111062005270102121452020126071C&lr=all&mime_type=video_mp4&net=0&pl=0&qs=0&rc=M2kzZTg6ZmRpODMzNGkzM0ApNWY7N2U7NWUzNzY7aTxmO2cpaHV2fWVuZDFwekBgNmU0cjQwY3JgLS1kLTBzc2JiLzNgXl5gYzA0MzZiXy86Y29zYlxmK2BtYmJeYA%3D%3D&vl=&vr=', 811, 26737, 3470, 'admin', '2021-11-06 20:05:27', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('a2df28f795cd4c99841b1e3534243347', '97aa1525-ec98-4031-aa77-c0e4735a4fff', '西陆强军号', 'https://v.douyin.com/RA5Mjqd/', '7月30日，是郭豪牺牲两周年！让我们铭记这个年轻的士兵！ #致敬英雄  #烈士', '@西陆强军号创作的原声', '', 'https://v13-a.douyinvod.com/caec97b5318238792b1e13c8b21606f9/61868293/video/tos/cn/tos-cn-ve-15/b602b4eecc3248008e4904b92bb27884/?a=1128&br=464&bt=464&cd=0%7C0%7C0&ch=96&cr=0&cs=0&cv=1&dr=0&ds=6&er=&ft=r5zpj77JWMFBMXcI3moVyXrK_&l=202111062026170102121462022D23EDE8&lr=all&mime_type=video_mp4&net=0&pl=0&qs=0&rc=MzNxPGp4aDh3djMzM2kzM0ApZWhnOGRmZmQ8N2Q5aTU3aWcpaHV2fWVuZDFwekBwYTUuci5sZ2VfLS1jLS9zczBeLTQwYi8vLzUvMS4vMmI6Y29zYlxmK2BtYmJeYA%3D%3D&vl=&vr=', 142, 40981, 133, 'admin', '2021-11-06 20:26:17', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('a480b6d2b078439bb2f7287b230a0f33', '6a62e9d2-433c-4201-8199-590431086924', '笑尼妹', 'https://v.douyin.com/RLgt8eU/', '#看一遍笑一遍 让你对象学学这精打细算', '@笑尼妹创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7023342780093188894.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0d00fg10000c5runorc77u7fmh2mhcg&ratio=720p&line=0', 1427, 10714, 6552, 'admin', '2021-10-27 19:22:06', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('ab5f8379a9404d4faa863be20c1efdfd', '97aa1525-ec98-4031-aa77-c0e4735a4fff', '小杨爱创作（视频创作者）', 'https://v.douyin.com/RA5ebBt/', '#搞笑视频 ', '@小杨爱创作（视频创作者）创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7027351807244487428.mp3', 'https://v5-coldb.douyinvod.com/07a85b89b5ccc6db3fddfbd8132e3bcb/618680da/video/tos/cn/tos-cn-ve-15-alinc2/2efad91f8a924eb282b8b000d6bfc1a5/?a=1128&br=769&bt=769&cd=0%7C0%7C0&ch=96&cr=0&cs=0&cv=1&dr=0&ds=6&er=&ft=r5zpj77JWMFBMXcI3mo~sXrK_&l=202111062019110102121361383532BDF8&lr=all&mime_type=video_mp4&net=0&pl=0&qs=0&rc=ajd0ZTg6Zmc1OTMzNGkzM0ApN2k1aGhkZjtkN2Q6Mzc1ZGcpaHV2fWVuZDFwekBzc2UzcjRfajBgLS1kLTBzczY0Yi0uXi00NjMtMWFiXzI6Y29zYlxmK2BtYmJeYA%3D%3D&vl=&vr=', 542, 3959, 2353, 'admin', '2021-11-06 20:19:11', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('b938fe51160f4379984d494258550bd1', '6a62e9d2-433c-4201-8199-590431086924', '甜七七✈️', 'https://v.douyin.com/RLGtVmP/', '外面零下8度 我怀里38度 你来选。 #泳池 #身材', '@甜七七✈️创作的原声', 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7023596452265904910.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c5sd7erc77u1tj3sp350&ratio=720p&line=0', 483, 17200, 1864, 'admin', '2021-10-27 20:05:46', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('cbe2b47558ea40cfa7414877f55f56bd', '6a62e9d2-433c-4201-8199-590431086924', '萱姐(专职合拍)', 'https://v.douyin.com/RLsJAnH/', '和 @oc酒吧副总东东 一起 #合拍 ', '@oc酒吧副总东东创作的原声', 'https://sf6-cdn-tos.douyinstatic.com/obj/mosaic-legacy/85c60005bdd5c19e5c9b', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c5shgtbc77u7cth1a56g&ratio=720p&line=0', 199, 2611, 1035, 'admin', '2021-10-27 19:36:17', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('d2585f3e894d49a6adaff7500b5f7dc0', '36cab16f-8a85-4055-9ef1-27bd875a6de0', '曙光音乐', 'https://v.douyin.com/RyYLKHc/', '这一次是真的，好久不见了#超燃bgm #音乐 #超燃 ', '@曙光音乐创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7025200443525876494.mp3', 'https://v3-cold2.douyinvod.com/aee3372cb571737323a516ba51d758ec/618b3664/video/tos/cn/tos-cn-ve-15-alinc2/699d183edb0e4f99bed6a1f0d8ec5243/?a=1128&br=1249&bt=1249&cd=0%7C0%7C0&ch=96&cr=0&cs=0&cv=1&dr=0&ds=6&er=&ft=5q8kcGmmnPNT2Fh7HhWwkXAGfdH.C738GBZc&l=2021111010023501021208122828504690&lr=all&mime_type=video_mp4&net=0&pl=0&qs=0&rc=MzNtdGU6ZmY7ODMzNGkzM0ApaTc3Zjc4N2UzNzVoNjY8M2cpaHV2fWVuZDFwekBpNGxhcjRvYHNgLS1kLTBzc2NgLmM2YWEwX2A2Li5fLjE6Y29zYlxmK2BtYmJeYA%3D%3D&vl=&vr=', 413, 14357, 1084, 'admin', '2021-11-10 10:02:35', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('ebc308c630f2494b85d673dd09d0ec5a', '6a62e9d2-433c-4201-8199-590431086924', '依婷', 'https://v.douyin.com/RLtYwqD/', '#抖音颜值 #可爱 #猫系女友 嘿嘿', '@依婷创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6972921004717525773.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c32cl7k0gv51pr7b5mt0&ratio=720p&line=0', 161, 7186, 130, 'admin', '2021-10-27 20:11:35', NULL, NULL, NULL);
INSERT INTO `loyj_douyin` VALUES ('f45b325c4bd34bb195c36e76ca178de4', '6a62e9d2-433c-4201-8199-590431086924', 'TING', 'https://v.douyin.com/RLGGAfJ/', '', '@秋一漫剪创作的原声', 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6984431396576037668.mp3', 'https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c5ok3gbc77u2cube4j40&ratio=720p&line=0', 191, 7812, 433, 'admin', '2021-10-27 20:08:08', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for loyj_douyin_class
-- ----------------------------
DROP TABLE IF EXISTS `loyj_douyin_class`;
CREATE TABLE `loyj_douyin_class`  (
                                      `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类主键ID',
                                      `pid` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '父类ID',
                                      `dy_class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '抖音分类名称',
                                      `order_num` int(4) NULL DEFAULT NULL COMMENT '显示顺序',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of loyj_douyin_class
-- ----------------------------
INSERT INTO `loyj_douyin_class` VALUES ('36cab16f-8a85-4055-9ef1-27bd875a6de0', '0', '好音乐', 5);
INSERT INTO `loyj_douyin_class` VALUES ('6a62e9d2-433c-4201-8199-590431086924', 'a6da2056-c0b9-4d46-bf0e-ff64c3003430', '美女', 1);
INSERT INTO `loyj_douyin_class` VALUES ('7c37cf38-30d6-4076-9a6d-53688de25e31', 'a6da2056-c0b9-4d46-bf0e-ff64c3003430', '酷酷的', 4);
INSERT INTO `loyj_douyin_class` VALUES ('97aa1525-ec98-4031-aa77-c0e4735a4fff', 'a6da2056-c0b9-4d46-bf0e-ff64c3003430', '搞笑', 3);
INSERT INTO `loyj_douyin_class` VALUES ('a6da2056-c0b9-4d46-bf0e-ff64c3003430', '0', '我的抖音', 1);
INSERT INTO `loyj_douyin_class` VALUES ('e0c8a90d-c922-4416-8c3c-37eb844d983a', 'a6da2056-c0b9-4d46-bf0e-ff64c3003430', '风景', 2);

-- ----------------------------
-- Table structure for loyj_douyin_photo
-- ----------------------------
DROP TABLE IF EXISTS `loyj_douyin_photo`;
CREATE TABLE `loyj_douyin_photo`  (
                                      `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键ID',
                                      `douyin_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '抖音外键ID',
                                      `photo_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '照片链接',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of loyj_douyin_photo
-- ----------------------------
INSERT INTO `loyj_douyin_photo` VALUES ('3c1ee781-0a5d-431e-a43d-e43cc608c9c0', '212960148dc24361872135374607c20f', 'https://p26-sign.douyinpic.com/tos-cn-i-0813/86ca80aa6fd149d6815baa9a31967a2e~noop.webp?x-expires=1638154800&x-signature=HQj8ok2m3P%2FQHIoCTV3EchbdYaM%3D&from=4257465056');
INSERT INTO `loyj_douyin_photo` VALUES ('790e81fd-0a4b-4021-b6d8-d1e205f6f2bb', '212960148dc24361872135374607c20f', 'https://p26-sign.douyinpic.com/tos-cn-i-0813/d189fca91db94d8c82a45278e4118637~noop.webp?x-expires=1638154800&x-signature=6Fax0bHS1apvR4bYVX%2Bt%2Fp014H4%3D&from=4257465056');
INSERT INTO `loyj_douyin_photo` VALUES ('f720a5fe-661d-47ac-b8ff-972d7b4d1f52', '212960148dc24361872135374607c20f', 'https://p3-sign.douyinpic.com/tos-cn-i-0813/14933a86128148089505e7d50cf5f8df~noop.webp?x-expires=1638154800&x-signature=bc8wTMxnV5nwqLJK3KSW49W2%2BQs%3D&from=4257465056');
INSERT INTO `loyj_douyin_photo` VALUES ('fcb46566-0e98-48d2-85da-f745bba67319', '212960148dc24361872135374607c20f', 'https://p3-sign.douyinpic.com/tos-cn-i-0813/13aeb243d1b5410080733f502f333e23~noop.webp?x-expires=1638154800&x-signature=dMWu1yWBT%2FOR2q6gl3Aqf1ftvp8%3D&from=4257465056');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
                                       `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                                       `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
                                       `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                       `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
                                       PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
                                       CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
                                   `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                                   `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '日历名称',
                                   `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
                                   PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '日历信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
                                       `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                                       `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
                                       `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                       `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'cron表达式',
                                       `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '时区',
                                       PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
                                       CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
                                        `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                                        `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度器实例id',
                                        `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
                                        `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                        `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度器实例名',
                                        `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
                                        `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
                                        `priority` int(11) NOT NULL COMMENT '优先级',
                                        `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '状态',
                                        `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '任务名称',
                                        `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '任务组名',
                                        `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否并发',
                                        `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
                                        PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '已触发的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
                                     `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                                     `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务名称',
                                     `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务组名',
                                     `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '相关介绍',
                                     `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '执行任务类名称',
                                     `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '是否持久化',
                                     `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '是否并发',
                                     `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '是否更新数据',
                                     `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '是否接受恢复执行',
                                     `job_data` blob NULL COMMENT '存放持久化job对象',
                                     PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '任务详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017BD040583878707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017BD040583878707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017BD040583878707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
                               `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                               `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '悲观锁名称',
                               PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
                                             `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                                             `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                             PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '暂停的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
                                         `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                                         `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '实例名称',
                                         `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
                                         `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
                                         PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '调度器状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'loyj1636525894285', 1636526137739, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
                                         `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                                         `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
                                         `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                         `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
                                         `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
                                         `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
                                         PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
                                         CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
                                          `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                                          `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
                                          `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                          `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
                                          `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
                                          `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
                                          `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
                                          `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
                                          `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
                                          `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
                                          `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
                                          `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
                                          `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
                                          `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
                                          PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
                                          CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
                                  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调度名称',
                                  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '触发器的名字',
                                  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '触发器所属组的名字',
                                  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
                                  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
                                  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '相关介绍',
                                  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
                                  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
                                  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
                                  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '触发器状态',
                                  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '触发器的类型',
                                  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
                                  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
                                  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '日程表名称',
                                  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
                                  `job_data` blob NULL COMMENT '存放持久化job对象',
                                  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
                                  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
                                  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '触发器详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1636525900000, -1, 5, 'PAUSED', 'CRON', 1636525894000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1636525905000, -1, 5, 'PAUSED', 'CRON', 1636525894000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1636525900000, -1, 5, 'PAUSED', 'CRON', 1636525894000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
                               `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
                               `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数名称',
                               `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数键名',
                               `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数键值',
                               `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
                               `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                               `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                               PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-09-10 23:08:03', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-09-10 23:08:03', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-09-10 23:08:03', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaOnOff', 'true', 'Y', 'admin', '2021-09-10 23:08:03', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2021-09-10 23:08:03', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
                             `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
                             `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
                             `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '祖级列表',
                             `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门名称',
                             `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
                             `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '负责人',
                             `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
                             `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 109 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
                                  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
                                  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
                                  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典标签',
                                  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典键值',
                                  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
                                  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
                                  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '表格回显样式',
                                  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
                                  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
                                  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
                                  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典名称',
                                  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
                                  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`dict_id`) USING BTREE,
                                  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
                            `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
                            `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '任务名称',
                            `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
                            `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调用目标字符串',
                            `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
                            `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
                            `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
                            `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
                            `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                            `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '备注信息',
                            PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2021-09-10 23:08:03', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2021-09-10 23:08:03', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2021-09-10 23:08:03', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
                                `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
                                `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务名称',
                                `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务组名',
                                `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调用目标字符串',
                                `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '日志信息',
                                `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
                                `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '异常信息',
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
                                   `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
                                   `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户账号',
                                   `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '登录IP地址',
                                   `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '登录地点',
                                   `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '浏览器类型',
                                   `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作系统',
                                   `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
                                   `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '提示消息',
                                   `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
                                   PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 136 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-09-10 23:12:22');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-09-22 15:16:39');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-09-22 16:14:42');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-09-22 16:29:53');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-09-22 17:47:49');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-09-25 20:02:15');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-09-26 19:26:45');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-09 15:35:37');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-10-09 17:56:01');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-10-11 16:03:01');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-11 16:03:05');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-11 17:29:22');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-10-20 16:05:44');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-10-20 16:05:47');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-10-20 16:05:57');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-10-20 16:06:07');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-20 16:06:10');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-20 17:50:17');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-22 23:20:58');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-10-23 00:08:05');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-23 00:08:09');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-26 19:46:12');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-26 21:04:36');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-27 19:04:07');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-29 18:47:00');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-10-29 21:04:41');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-29 21:04:47');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-30 10:27:26');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-30 12:10:12');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-10-30 13:53:36');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-11-06 20:00:28');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-11-08 09:02:04');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-11-09 17:23:26');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-11-10 10:01:30');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-11-10 10:01:34');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-11-10 10:06:49');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-11-10 14:34:10');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
                             `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
                             `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名称',
                             `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
                             `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
                             `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '路由地址',
                             `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组件路径',
                             `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '路由参数',
                             `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
                             `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
                             `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
                             `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
                             `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
                             `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '#' COMMENT '菜单图标',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '备注',
                             PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2008 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2021-09-10 23:08:02', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, '', 1, 0, 'M', '1', '0', '', 'monitor', 'admin', '2021-09-10 23:08:02', 'admin', '2021-10-20 16:53:29', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2021-09-10 23:08:02', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, '', 0, 0, 'M', '0', '0', '', 'guide', 'admin', '2021-09-10 23:08:02', '', NULL, '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2021-09-10 23:08:02', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2021-09-10 23:08:02', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2021-09-10 23:08:02', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2021-09-10 23:08:02', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2021-09-10 23:08:02', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2021-09-10 23:08:02', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2021-09-10 23:08:02', 'admin', '2021-10-30 10:27:53', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '1', '0', 'system:notice:list', 'message', 'admin', '2021-09-10 23:08:02', 'admin', '2021-10-20 16:55:29', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '1', '0', '', 'log', 'admin', '2021-09-10 23:08:02', 'admin', '2021-10-20 16:55:24', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2021-09-10 23:08:02', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2021-09-10 23:08:02', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2021-09-10 23:08:02', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2021-09-10 23:08:02', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2021-09-10 23:08:02', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '1', '0', 'tool:build:list', 'build', 'admin', '2021-09-10 23:08:02', 'admin', '2021-10-20 16:54:32', '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2021-09-10 23:08:02', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '1', '0', 'tool:swagger:list', 'swagger', 'admin', '2021-09-10 23:08:02', 'admin', '2021-10-20 16:54:36', '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2021-09-10 23:08:02', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2021-09-10 23:08:02', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2001, '爬虫模块', 0, 0, 'spider', NULL, NULL, 1, 0, 'M', '0', '0', '', 'bug', 'admin', '2021-09-10 23:15:10', 'admin', '2021-09-10 23:15:46', '');
INSERT INTO `sys_menu` VALUES (2003, '抖音解析', 2001, 1, 'douyin', 'spider/douyin/index', NULL, 1, 0, 'C', '0', '0', 'spider:douyin:list', '#', 'admin', '2021-09-10 23:40:29', '', NULL, '抖音解析菜单');
INSERT INTO `sys_menu` VALUES (2004, '抖音解析查询', 2003, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'spider:douyin:query', '#', 'admin', '2021-09-10 23:40:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '抖音解析新增', 2003, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'spider:douyin:add', '#', 'admin', '2021-09-10 23:40:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2006, '抖音解析修改', 2003, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'spider:douyin:edit', '#', 'admin', '2021-09-10 23:40:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2007, '抖音解析删除', 2003, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'spider:douyin:remove', '#', 'admin', '2021-09-10 23:40:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2008, '抖音解析导出', 2003, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'spider:douyin:export', '#', 'admin', '2021-09-10 23:40:29', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
                               `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
                               `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告标题',
                               `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
                               `notice_content` longblob NULL COMMENT '公告内容',
                               `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
                               `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                               `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                               PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2021-09-10 23:08:03', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2021-09-10 23:08:03', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
                                 `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
                                 `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '模块标题',
                                 `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
                                 `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '方法名称',
                                 `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求方式',
                                 `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
                                 `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作人员',
                                 `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门名称',
                                 `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求URL',
                                 `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '主机地址',
                                 `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作地点',
                                 `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求参数',
                                 `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '返回参数',
                                 `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
                                 `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '错误消息',
                                 `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
                                 PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 278 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"bug\",\"orderNum\":\"0\",\"menuName\":\"爬虫模块\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"loyj\",\"component\":\"loyj/spider/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"loyj:spider:index\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-10 23:14:26');
INSERT INTO `sys_oper_log` VALUES (101, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2000', '127.0.0.1', '内网IP', '{menuId=2000}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-10 23:14:35');
INSERT INTO `sys_oper_log` VALUES (102, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"bug\",\"orderNum\":\"0\",\"menuName\":\"爬虫模块\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"spider\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-10 23:15:10');
INSERT INTO `sys_oper_log` VALUES (103, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"bug\",\"orderNum\":\"0\",\"menuName\":\"爬虫模块\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"loyj\",\"children\":[],\"createTime\":1631286910000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-10 23:15:37');
INSERT INTO `sys_oper_log` VALUES (104, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"bug\",\"orderNum\":\"0\",\"menuName\":\"爬虫模块\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"spider\",\"children\":[],\"createTime\":1631286910000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-10 23:15:46');
INSERT INTO `sys_oper_log` VALUES (105, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":\"1\",\"menuName\":\"抖音解析\",\"params\":{},\"parentId\":2001,\"isCache\":\"0\",\"path\":\"spider\",\"component\":\"spider/douyin/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"spider:douyin:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-10 23:17:37');
INSERT INTO `sys_oper_log` VALUES (106, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'loyj_douyin', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-10 23:32:15');
INSERT INTO `sys_oper_log` VALUES (107, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"loyj\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键ID\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631287935000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"ShareUrl\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"shareUrl\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"抖音分享的URL\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631287935000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"share_url\"},{\"capJavaField\":\"Title\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"title\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"抖音标题\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(300)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631287935000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"title\"},{\"capJavaField\":\"NoWatermarkUrl\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"noWatermarkUrl\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"无水印URL\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"va', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-10 23:35:51');
INSERT INTO `sys_oper_log` VALUES (108, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-09-10 23:36:02');
INSERT INTO `sys_oper_log` VALUES (109, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2002', '127.0.0.1', '内网IP', '{menuId=2002}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-10 23:40:04');
INSERT INTO `sys_oper_log` VALUES (110, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1631291351685,\"remark\":\"123\",\"shareUrl\":\"123\",\"params\":{}}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\java小项目\\RuoYi-Vue\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( share_url,                                       create_by,             create_time,                                       remark )           values ( ?,                                       ?,             ?,                                       ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-09-11 00:29:11');
INSERT INTO `sys_oper_log` VALUES (111, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1631291421865,\"remark\":\"123\",\"shareUrl\":\"123\",\"id\":\"68ba227bdfaa45449e1e3dedb8c82ba3\",\"params\":{}}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'no_watermark_url\' doesn\'t have a default value\r\n### The error may exist in file [E:\\java小项目\\RuoYi-Vue\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( id,             share_url,                                       create_by,             create_time,                                       remark )           values ( ?,             ?,                                       ?,             ?,                                       ? )\r\n### Cause: java.sql.SQLException: Field \'no_watermark_url\' doesn\'t have a default value\n; Field \'no_watermark_url\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'no_watermark_url\' doesn\'t have a default value', '2021-09-11 00:30:21');
INSERT INTO `sys_oper_log` VALUES (112, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/loyj_douyin', '127.0.0.1', '内网IP', '{tableName=loyj_douyin}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 15:17:05');
INSERT INTO `sys_oper_log` VALUES (113, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"loyj\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键ID\",\"updateTime\":1631288151000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631287935000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"ShareUrl\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"shareUrl\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"抖音分享的URL\",\"updateTime\":1631288151000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631287935000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"share_url\"},{\"capJavaField\":\"Author\",\"usableColumn\":false,\"columnId\":10,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"author\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"作者\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1632295025000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"author\"},{\"capJavaField\":\"Title\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"title\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"抖音标题\",\"isQuery\":\"1\",\"updateTime\":1631288151000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"quer', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 15:20:06');
INSERT INTO `sys_oper_log` VALUES (114, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/loyj_douyin', '127.0.0.1', '内网IP', '{tableName=loyj_douyin}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 15:21:00');
INSERT INTO `sys_oper_log` VALUES (115, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"loyj\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键ID\",\"updateTime\":1632295206000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631287935000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"ShareUrl\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"shareUrl\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"抖音分享的URL\",\"updateTime\":1632295206000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631287935000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"share_url\"},{\"capJavaField\":\"Author\",\"usableColumn\":false,\"columnId\":10,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"author\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"作者\",\"isQuery\":\"1\",\"updateTime\":1632295206000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1632295025000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"author\"},{\"capJavaField\":\"Title\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"title\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"抖音标题\",\"isQuery\":\"1\",\"updateTime\":1632295206000,\"sort\":3,\"list\":true,\"params\":{', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 15:22:11');
INSERT INTO `sys_oper_log` VALUES (116, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-09-22 15:22:15');
INSERT INTO `sys_oper_log` VALUES (117, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, '', '2021-09-22 15:32:20');
INSERT INTO `sys_oper_log` VALUES (118, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, '', '2021-09-22 15:43:12');
INSERT INTO `sys_oper_log` VALUES (119, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, '', '2021-09-22 15:48:41');
INSERT INTO `sys_oper_log` VALUES (120, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, '', '2021-09-22 15:55:35');
INSERT INTO `sys_oper_log` VALUES (121, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, 'java.text.ParseException: Unparseable date: \"1632218400\"', '2021-09-22 15:57:35');
INSERT INTO `sys_oper_log` VALUES (122, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'share_url\' at row 1\r\n### The error may exist in file [E:\\java小项目\\RuoYi-Vue\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( share_url )           values ( ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'share_url\' at row 1\n; Data truncation: Data too long for column \'share_url\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'share_url\' at row 1', '2021-09-22 16:01:54');
INSERT INTO `sys_oper_log` VALUES (123, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'share_url\' at row 1\r\n### The error may exist in file [E:\\java小项目\\RuoYi-Vue\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( share_url )           values ( ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'share_url\' at row 1\n; Data truncation: Data too long for column \'share_url\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'share_url\' at row 1', '2021-09-22 16:02:05');
INSERT INTO `sys_oper_log` VALUES (124, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'share_url\' at row 1\r\n### The error may exist in file [E:\\java小项目\\RuoYi-Vue\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( share_url )           values ( ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'share_url\' at row 1\n; Data truncation: Data too long for column \'share_url\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'share_url\' at row 1', '2021-09-22 16:04:24');
INSERT INTO `sys_oper_log` VALUES (125, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\java小项目\\RuoYi-Vue\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( share_url )           values ( ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-09-22 16:06:20');
INSERT INTO `sys_oper_log` VALUES (126, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\java小项目\\RuoYi-Vue\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( share_url )           values ( ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-09-22 16:08:02');
INSERT INTO `sys_oper_log` VALUES (127, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\java小项目\\RuoYi-Vue\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( share_url )           values ( ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-09-22 16:09:46');
INSERT INTO `sys_oper_log` VALUES (128, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"6.15 oDh:/ 史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ %lol   https://v.douyin.com/dPUtWB7/ 複制此链接，打开Dou音搜索，直接观看視频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 16:12:05');
INSERT INTO `sys_oper_log` VALUES (129, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/1de71d8ab9834ba6b79b5c0842c73aff', '127.0.0.1', '内网IP', '{ids=1de71d8ab9834ba6b79b5c0842c73aff}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 16:18:39');
INSERT INTO `sys_oper_log` VALUES (130, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dPUtWB7\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-09-22 16:18:44');
INSERT INTO `sys_oper_log` VALUES (131, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dPUtWB7\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-09-22 16:19:03');
INSERT INTO `sys_oper_log` VALUES (132, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dPUtWB7\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-09-22 16:20:11');
INSERT INTO `sys_oper_log` VALUES (133, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dPUtWB7\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-09-22 16:20:36');
INSERT INTO `sys_oper_log` VALUES (134, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dPUtWB7\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-09-22 16:31:01');
INSERT INTO `sys_oper_log` VALUES (135, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dPUtWB7\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-09-22 16:33:15');
INSERT INTO `sys_oper_log` VALUES (136, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dPUtWB7/\"}', 'null', 1, 'Cannot format given Object as a Date', '2021-09-22 16:34:06');
INSERT INTO `sys_oper_log` VALUES (137, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dPUtWB7/\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 16:35:18');
INSERT INTO `sys_oper_log` VALUES (138, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/d86098382ba14d9fb8b1e50e8668e7c4', '127.0.0.1', '内网IP', '{ids=d86098382ba14d9fb8b1e50e8668e7c4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 16:49:13');
INSERT INTO `sys_oper_log` VALUES (139, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dPUtWB7/\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 16:49:18');
INSERT INTO `sys_oper_log` VALUES (140, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/aa317e1ef0264199addfdb52d7690286', '127.0.0.1', '内网IP', '{ids=aa317e1ef0264199addfdb52d7690286}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 16:50:05');
INSERT INTO `sys_oper_log` VALUES (141, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dPUtWB7/\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 16:50:09');
INSERT INTO `sys_oper_log` VALUES (142, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"0.56 YmQ:/ %回忆杀  %音乐 %2021最火歌曲 @DOU+小助手 带你们回忆一波2021年的音乐  https://v.douyin.com/dPm6YjK/ 复淛此链接，打开Dou搜索，直接觀kan視频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 16:53:10');
INSERT INTO `sys_oper_log` VALUES (143, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1624033000,\"author\":\"东东（宝藏音乐）\",\"musicUrl\":\"https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6975172931651521288.mp3\",\"diggCount\":560563,\"updateTime\":1632304992331,\"params\":{},\"title\":\"#回忆杀  #音乐 #2021最火歌曲 @DOU+小助手 带你们回忆一波2021年的音乐\",\"commentCount\":11767,\"musicTitle\":\"@东东（宝藏音乐）创作的原声\",\"shareCount\":21059,\"createBy\":\"admin\",\"createTime\":1632300790000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c36cf3fhdcrm6bciju30&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dPm6YjK/\",\"id\":\"016799b103d04f7ca5c91a887c671043\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:03:12');
INSERT INTO `sys_oper_log` VALUES (144, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1632218000,\"author\":\"lol每日趣事\",\"diggCount\":9904,\"updateTime\":1632305010948,\"params\":{},\"title\":\"史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ #lol \",\"commentCount\":1694,\"shareCount\":3412,\"createBy\":\"admin\",\"createTime\":1632300609000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c54qqgbc77ueed3k87ag&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dPUtWB7/\",\"id\":\"21c4ed388a2b4c32aea4329ce381767f\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:03:30');
INSERT INTO `sys_oper_log` VALUES (145, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1624033000,\"author\":\"东东（宝藏音乐）\",\"musicUrl\":\"https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6975172931651521288.mp3\",\"diggCount\":560563,\"updateTime\":1632305016169,\"params\":{},\"title\":\"#回忆杀  #音乐 #2021最火歌曲 @DOU+小助手 带你们回忆一波2021年的音乐\",\"commentCount\":11767,\"musicTitle\":\"@东东（宝藏音乐）创作的原声\",\"shareCount\":21059,\"createBy\":\"admin\",\"createTime\":1632300790000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c36cf3fhdcrm6bciju30&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dPm6YjK/\",\"id\":\"016799b103d04f7ca5c91a887c671043\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:03:36');
INSERT INTO `sys_oper_log` VALUES (146, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"场，国产电影《红泥屋》 %影视解说   https://v.douyin.com/dP9XCfg/ 復制此lian接，da开Dou音搜索，直接观看視频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:04:46');
INSERT INTO `sys_oper_log` VALUES (147, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1631786000,\"author\":\"泪目讲电影\",\"diggCount\":1479921,\"updateTime\":1632305102714,\"params\":{},\"title\":\"小伙意外捡到漂亮女孩，花光积蓄供她上学，没想到却落个如此下场，国产电影《红泥屋》 #影视解说 \",\"commentCount\":52381,\"shareCount\":87765,\"createBy\":\"admin\",\"createTime\":1632305087000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c51hbobc77udugv72klg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dP9XCfg/\",\"id\":\"2f77d051e0b94a52a1280c9a9697fec9\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:05:02');
INSERT INTO `sys_oper_log` VALUES (148, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1631786000,\"author\":\"泪目讲电影\",\"diggCount\":1479921,\"updateTime\":1632305112681,\"params\":{},\"title\":\"小伙意外捡到漂亮女孩，花光积蓄供她上学，没想到却落个如此下场，国产电影《红泥屋》 #影视解说 \",\"commentCount\":52381,\"shareCount\":87765,\"createBy\":\"admin\",\"createTime\":1632305087000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c51hbobc77udugv72klg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dP9XCfg/\",\"id\":\"2f77d051e0b94a52a1280c9a9697fec9\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:05:12');
INSERT INTO `sys_oper_log` VALUES (149, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1631786000,\"author\":\"泪目讲电影\",\"diggCount\":1479921,\"updateTime\":1632305114136,\"params\":{},\"title\":\"小伙意外捡到漂亮女孩，花光积蓄供她上学，没想到却落个如此下场，国产电影《红泥屋》 #影视解说 \",\"commentCount\":52381,\"shareCount\":87765,\"createBy\":\"admin\",\"createTime\":1632305087000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c51hbobc77udugv72klg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dP9XCfg/\",\"id\":\"2f77d051e0b94a52a1280c9a9697fec9\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:05:14');
INSERT INTO `sys_oper_log` VALUES (150, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1631786000,\"author\":\"泪目讲电影\",\"diggCount\":1479921,\"updateTime\":1632305115280,\"params\":{},\"title\":\"小伙意外捡到漂亮女孩，花光积蓄供她上学，没想到却落个如此下场，国产电影《红泥屋》 #影视解说 \",\"commentCount\":52381,\"shareCount\":87765,\"createBy\":\"admin\",\"createTime\":1632305087000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c51hbobc77udugv72klg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dP9XCfg/\",\"id\":\"2f77d051e0b94a52a1280c9a9697fec9\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:05:15');
INSERT INTO `sys_oper_log` VALUES (151, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1631786000,\"author\":\"泪目讲电影\",\"diggCount\":1479921,\"updateTime\":1632305147712,\"params\":{},\"title\":\"小伙意外捡到漂亮女孩，花光积蓄供她上学，没想到却落个如此下场，国产电影《红泥屋》 #影视解说 \",\"commentCount\":52381,\"shareCount\":87765,\"createBy\":\"admin\",\"createTime\":1632305087000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c51hbobc77udugv72klg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dP9XCfg/\",\"id\":\"2f77d051e0b94a52a1280c9a9697fec9\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:05:47');
INSERT INTO `sys_oper_log` VALUES (152, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1631786000,\"author\":\"泪目讲电影\",\"diggCount\":1479921,\"updateTime\":1632305148735,\"params\":{},\"title\":\"小伙意外捡到漂亮女孩，花光积蓄供她上学，没想到却落个如此下场，国产电影《红泥屋》 #影视解说 \",\"commentCount\":52381,\"shareCount\":87765,\"createBy\":\"admin\",\"createTime\":1632305087000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c51hbobc77udugv72klg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dP9XCfg/\",\"id\":\"2f77d051e0b94a52a1280c9a9697fec9\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:05:48');
INSERT INTO `sys_oper_log` VALUES (153, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1631786000,\"author\":\"泪目讲电影\",\"diggCount\":1479921,\"updateTime\":1632305149000,\"params\":{},\"title\":\"小伙意外捡到漂亮女孩，花光积蓄供她上学，没想到却落个如此下场，国产电影《红泥屋》 #影视解说 \",\"commentCount\":52381,\"shareCount\":87765,\"createBy\":\"admin\",\"createTime\":1632305087000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c51hbobc77udugv72klg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dP9XCfg/\",\"id\":\"2f77d051e0b94a52a1280c9a9697fec9\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:06:52');
INSERT INTO `sys_oper_log` VALUES (154, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1631786000,\"author\":\"泪目讲电影\",\"diggCount\":1480446,\"updateTime\":1632305212000,\"params\":{},\"title\":\"小伙意外捡到漂亮女孩，花光积蓄供她上学，没想到却落个如此下场，国产电影《红泥屋》 #影视解说 \",\"commentCount\":52397,\"shareCount\":87790,\"createBy\":\"admin\",\"createTime\":1632305087000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c51hbobc77udugv72klg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dP9XCfg/\",\"id\":\"2f77d051e0b94a52a1280c9a9697fec9\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-09-22 18:07:05');
INSERT INTO `sys_oper_log` VALUES (155, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://www.iesdouyin.com/share/video/7006301711048510728/?region=&mid=7006301877408877348&u_code=48&titleType=title&did=MS4wLjABAAAANwkJuWIRFOzg5uCpDRpMj4OX-QryoDgn-yYlXQnRwQQ&iid=MS4wLjABAAAANwkJuWIRFOzg5uCpDRpMj4OX-QryoDgn-yYlXQnRwQQ&with_sec_did=1\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-10-09 16:30:17');
INSERT INTO `sys_oper_log` VALUES (156, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://www.iesdouyin.com/share/video/7006301711048510728/?region=&mid=7006301877408877348&u_code=48&titleType=title&did=MS4wLjABAAAANwkJuWIRFOzg5uCpDRpMj4OX-QryoDgn-yYlXQnRwQQ&iid=MS4wLjABAAAANwkJuWIRFOzg5uCpDRpMj4OX-QryoDgn-yYlXQnRwQQ&with_sec_did=1\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-10-09 16:31:01');
INSERT INTO `sys_oper_log` VALUES (157, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1624033000,\"author\":\"东东（宝藏音乐）\",\"musicUrl\":\"https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6975172931651521288.mp3\",\"diggCount\":560563,\"updateTime\":1632305016000,\"params\":{},\"title\":\"#回忆杀  #音乐 #2021最火歌曲 @DOU+小助手 带你们回忆一波2021年的音乐\",\"commentCount\":11767,\"musicTitle\":\"@东东（宝藏音乐）创作的原声\",\"shareCount\":21059,\"createBy\":\"admin\",\"createTime\":1632300790000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c36cf3fhdcrm6bciju30&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dPm6YjK/\",\"id\":\"016799b103d04f7ca5c91a887c671043\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-09 16:32:58');
INSERT INTO `sys_oper_log` VALUES (158, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1632218000,\"author\":\"lol每日趣事\",\"diggCount\":9904,\"updateTime\":1632305011000,\"params\":{},\"title\":\"史上最恶心套路，开局5人直接送死，只要连送35次就能获胜！ #lol \",\"commentCount\":1694,\"shareCount\":3412,\"createBy\":\"admin\",\"createTime\":1632300609000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c54qqgbc77ueed3k87ag&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dPUtWB7/\",\"id\":\"21c4ed388a2b4c32aea4329ce381767f\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-09 16:33:02');
INSERT INTO `sys_oper_log` VALUES (159, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1631786000,\"author\":\"泪目讲电影\",\"diggCount\":1480501,\"updateTime\":1632305225000,\"params\":{},\"title\":\"小伙意外捡到漂亮女孩，花光积蓄供她上学，没想到却落个如此下场，国产电影《红泥屋》 #影视解说 \",\"commentCount\":52399,\"shareCount\":87791,\"createBy\":\"admin\",\"createTime\":1632305087000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c51hbobc77udugv72klg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dP9XCfg/\",\"id\":\"2f77d051e0b94a52a1280c9a9697fec9\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-09 16:33:04');
INSERT INTO `sys_oper_log` VALUES (160, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"2.82 JVY:/ %点燃一根烟%经典音乐%音乐  https://v.douyin.com/dGRx1Qh/ 复製Ci链接，咑开Dou音搜索，値接观看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-09 17:34:13');
INSERT INTO `sys_oper_log` VALUES (161, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"全场，实力强悍，再次让你们见到什么叫实力%音乐 %音乐推荐 %热歌热门分享  https://v.douyin.com/dG89587/ 复制此链接，答kaiDou姻搜索，直接观看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-09 17:50:39');
INSERT INTO `sys_oper_log` VALUES (162, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/21c4ed388a2b4c32aea4329ce381767f', '127.0.0.1', '内网IP', '{ids=21c4ed388a2b4c32aea4329ce381767f}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-09 18:05:48');
INSERT INTO `sys_oper_log` VALUES (163, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/2f77d051e0b94a52a1280c9a9697fec9', '127.0.0.1', '内网IP', '{ids=2f77d051e0b94a52a1280c9a9697fec9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-09 18:05:50');
INSERT INTO `sys_oper_log` VALUES (164, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/dGLXCJj/ 覆制此链\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-09 18:06:26');
INSERT INTO `sys_oper_log` VALUES (165, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"2.02 teb:/ %北国之春%音乐%怀旧%经典  https://v.douyin.com/dGLmsWp/ 複zhi此链接，答开Dou愔搜索，直接观看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-09 18:06:53');
INSERT INTO `sys_oper_log` VALUES (166, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1624033000,\"author\":\"东东（宝藏音乐）\",\"musicUrl\":\"https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/6975172931651521288.mp3\",\"diggCount\":595279,\"updateTime\":1633768379000,\"params\":{},\"title\":\"#回忆杀  #音乐 #2021最火歌曲 @DOU+小助手 带你们回忆一波2021年的音乐\",\"commentCount\":12570,\"musicTitle\":\"@东东（宝藏音乐）创作的原声\",\"shareCount\":22952,\"createBy\":\"admin\",\"createTime\":1632300790000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c36cf3fhdcrm6bciju30&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dPm6YjK/\",\"id\":\"016799b103d04f7ca5c91a887c671043\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:17:39');
INSERT INTO `sys_oper_log` VALUES (167, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1633664000,\"author\":\"倾听音乐（音乐库）（收徒-u盘）\",\"musicUrl\":\"https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7015782653232155429.mp3\",\"diggCount\":5978,\"params\":{},\"title\":\"#北国之春#音乐#怀旧#经典\",\"commentCount\":94,\"musicTitle\":\"@倾听音乐（音乐库）创作的原声\",\"shareCount\":417,\"createBy\":\"admin\",\"createTime\":1633774013000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c5frtfjc77u3to5e1930&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dGLmsWp/\",\"id\":\"7c14ad6110344334ad8ab8926e5a8b25\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:17:43');
INSERT INTO `sys_oper_log` VALUES (168, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1633484000,\"author\":\"倾听音乐（音乐库）（收徒-u盘）\",\"musicUrl\":\"https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7015763795206900516.mp3\",\"diggCount\":40943,\"params\":{},\"title\":\"#点燃一根烟#经典音乐#音乐\",\"commentCount\":448,\"musicTitle\":\"@倾听音乐（音乐库）（收徒-u盘）创作的原声\",\"shareCount\":5774,\"createBy\":\"admin\",\"createTime\":1633772054000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c5efvm3c77u7783gnldg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dGRx1Qh/\",\"id\":\"8ac92fec3b1a4cd99c2ce741dd7d659f\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:17:47');
INSERT INTO `sys_oper_log` VALUES (169, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1633305000,\"author\":\"音乐库（🚗载优盘）\",\"musicUrl\":\"https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7014823577396464392.mp3\",\"diggCount\":77240,\"params\":{},\"title\":\"实力唱将#张杰 深情演绎#无情的情书 高音炸裂，震撼全场，实力强悍，再次让你们见到什么叫实力#音乐 #音乐推荐 #热歌热门分享\",\"commentCount\":3667,\"musicTitle\":\"@悠悠音乐（车载🎶盘）创作的原声\",\"shareCount\":3997,\"createBy\":\"admin\",\"createTime\":1633773040000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c5d46bjc77uf1iiqlftg&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dG89587/\",\"id\":\"bef0ba9d53c14a549869c87eace54f9a\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:17:49');
INSERT INTO `sys_oper_log` VALUES (170, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1633273000,\"author\":\"音乐库车载优盘\",\"musicUrl\":\"https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7014858050708687652.mp3\",\"diggCount\":103250,\"params\":{},\"title\":\"愿你只识曲中意，不做曲中人……#音乐#一路向北#完整版@DOU+小助手 #我要上热门 \",\"commentCount\":1671,\"musicTitle\":\"@音乐库车载优盘创作的原声\",\"shareCount\":19421,\"createBy\":\"admin\",\"createTime\":1633773986000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0d00fg10000c5cseujc77uee3fnk0c0&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/dGLXCJj/\",\"id\":\"ff34c4c8f8384dc8b87fc7f79c4c0c3f\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:17:51');
INSERT INTO `sys_oper_log` VALUES (171, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"monitor\",\"orderNum\":\"2\",\"menuName\":\"系统监控\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"monitor\",\"children\":[],\"createTime\":1631286482000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:53:29');
INSERT INTO `sys_oper_log` VALUES (172, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"build\",\"orderNum\":\"1\",\"menuName\":\"表单构建\",\"params\":{},\"parentId\":3,\"isCache\":\"0\",\"path\":\"build\",\"component\":\"tool/build/index\",\"children\":[],\"createTime\":1631286482000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":114,\"menuType\":\"C\",\"perms\":\"tool:build:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:54:32');
INSERT INTO `sys_oper_log` VALUES (173, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"swagger\",\"orderNum\":\"3\",\"menuName\":\"系统接口\",\"params\":{},\"parentId\":3,\"isCache\":\"0\",\"path\":\"swagger\",\"component\":\"tool/swagger/index\",\"children\":[],\"createTime\":1631286482000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":116,\"menuType\":\"C\",\"perms\":\"tool:swagger:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:54:36');
INSERT INTO `sys_oper_log` VALUES (174, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"log\",\"orderNum\":\"9\",\"menuName\":\"日志管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"log\",\"component\":\"\",\"children\":[],\"createTime\":1631286482000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":108,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:55:24');
INSERT INTO `sys_oper_log` VALUES (175, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"message\",\"orderNum\":\"8\",\"menuName\":\"通知公告\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"notice\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1631286482000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":107,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:55:29');
INSERT INTO `sys_oper_log` VALUES (176, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"query\":\"\",\"icon\":\"edit\",\"orderNum\":\"7\",\"menuName\":\"参数设置\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"config\",\"component\":\"system/config/index\",\"children\":[],\"createTime\":1631286482000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":106,\"menuType\":\"C\",\"perms\":\"system:config:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-20 16:55:35');
INSERT INTO `sys_oper_log` VALUES (177, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'loyj_douyin_photo', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-22 23:27:16');
INSERT INTO `sys_oper_log` VALUES (178, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-10-22 23:27:21');
INSERT INTO `sys_oper_log` VALUES (179, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"loyj\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":23,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键ID\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1634916436000,\"tableId\":2,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Photourl\",\"usableColumn\":false,\"columnId\":24,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"photourl\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"照片链接\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1634916436000,\"isEdit\":\"1\",\"tableId\":2,\"pk\":false,\"columnName\":\"photoUrl\"}],\"businessName\":\"photo\",\"moduleName\":\"loyj\",\"className\":\"DouyinPhoto\",\"tableName\":\"loyj_douyin_photo\",\"crud\":true,\"options\":\"{}\",\"genType\":\"0\",\"packageName\":\"com.ruoyi.loyj\",\"functionName\":\"抖音照片\",\"tree\":false,\"tableComment\":\"抖音照片表\",\"params\":{},\"tplCategory\":\"crud\",\"tableId\":2,\"genPath\":\"/\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-22 23:33:34');
INSERT INTO `sys_oper_log` VALUES (180, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-10-22 23:33:36');
INSERT INTO `sys_oper_log` VALUES (181, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-10-22 23:33:45');
INSERT INTO `sys_oper_log` VALUES (182, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/RJ1eA3d/\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Table \'loyj.loyj_doyin_photo\' doesn\'t exist\r\n### The error may exist in file [E:\\java小项目\\RuoYi-Vue\\loyj\\target\\classes\\mapper\\DouyinPhotoMapper.xml]\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_doyin_photo( id, photoUrl) values                        ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)\r\n### Cause: java.sql.SQLSyntaxErrorException: Table \'loyj.loyj_doyin_photo\' doesn\'t exist\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Table \'loyj.loyj_doyin_photo\' doesn\'t exist', '2021-10-23 00:08:19');
INSERT INTO `sys_oper_log` VALUES (183, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/RJ1eA3d/\"}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'id\' at row 1\r\n### The error may exist in file [E:\\java小项目\\RuoYi-Vue\\loyj\\target\\classes\\mapper\\DouyinPhotoMapper.xml]\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin_photo( id, photoUrl) values                        ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)          ,              ( ?, ?)\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'id\' at row 1\n; Data truncation: Data too long for column \'id\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'id\' at row 1', '2021-10-23 00:11:25');
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          INSERT INTO `sys_oper_log` VALUES (184, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/RJ1eA3d/\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-23 00:12:06');
INSERT INTO `sys_oper_log` VALUES (185, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1634882000,\"author\":\"时尚美图\",\"musicUrl\":\"https://sf3-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/d99a4a9491684953adb9d8201d8efa32\",\"diggCount\":15,\"params\":{},\"title\":\"#美女图片 #高清 #身材 \",\"commentCount\":0,\"musicTitle\":\"Trip\",\"shareCount\":10,\"createBy\":\"admin\",\"createTime\":1634919127000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=https://sf3-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/d99a4a9491684953adb9d8201d8efa32&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/RJ1eA3d/\",\"id\":\"4f92ee5a2b5e487ea5104ea743dcea45\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-23 00:30:09');
INSERT INTO `sys_oper_log` VALUES (186, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/4f92ee5a2b5e487ea5104ea743dcea45', '127.0.0.1', '内网IP', '{ids=4f92ee5a2b5e487ea5104ea743dcea45}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-23 00:30:30');
INSERT INTO `sys_oper_log` VALUES (187, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"https://v.douyin.com/RJ1eA3d/\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-23 00:30:35');
INSERT INTO `sys_oper_log` VALUES (188, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"3.84 dNW:/ 再就业的万能充%万能充 %苹果耳机%耳机%充电  https://v.douyin.com/R8XCco2/ 复制佌链接，打幵Dou音搜索，直接觀看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-26 19:47:56');
INSERT INTO `sys_oper_log` VALUES (189, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-10-26 19:59:59');
INSERT INTO `sys_oper_log` VALUES (190, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/loyj_douyin_photo', '127.0.0.1', '内网IP', '{tableName=loyj_douyin_photo}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-26 20:02:20');
INSERT INTO `sys_oper_log` VALUES (191, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"loyj\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":23,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键ID\",\"updateTime\":1634916814000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1634916436000,\"tableId\":2,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Photourl\",\"usableColumn\":false,\"columnId\":24,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"photourl\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"照片链接\",\"isQuery\":\"1\",\"updateTime\":1634916814000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1634916436000,\"isEdit\":\"1\",\"tableId\":2,\"pk\":false,\"columnName\":\"photoUrl\"},{\"capJavaField\":\"Douyinid\",\"usableColumn\":false,\"columnId\":25,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"douyinid\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"抖音外键ID\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1635249740000,\"isEdit\":\"1\",\"tableId\":2,\"pk\":false,\"columnName\":\"douyinId\"}],\"businessName\":\"photo\",\"moduleName\":\"loyj\",\"className\":\"DouyinPhoto\",\"tableName\":\"loyj_douyin_photo\",\"crud\":true,\"options\":\"{}\",\"genType\":\"0\",\"packageName\":\"com.ruoyi.loyj\",\"functionName\":\"抖音照片\",\"tree\":false,\"tableComment\":\"抖音照片表\",\"params\":{},\"tplCategory\":\"crud\",\"tableId\":2,\"genPath\":\"/\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-26 20:02:30');
INSERT INTO `sys_oper_log` VALUES (192, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-10-26 20:02:33');
INSERT INTO `sys_oper_log` VALUES (193, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-10-26 20:02:53');
INSERT INTO `sys_oper_log` VALUES (194, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/ff34c4c8f8384dc8b87fc7f79c4c0c3f', '127.0.0.1', '内网IP', '{ids=ff34c4c8f8384dc8b87fc7f79c4c0c3f}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:12:19');
INSERT INTO `sys_oper_log` VALUES (195, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/8ac92fec3b1a4cd99c2ce741dd7d659f', '127.0.0.1', '内网IP', '{ids=8ac92fec3b1a4cd99c2ce741dd7d659f}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:12:22');
INSERT INTO `sys_oper_log` VALUES (196, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/585e81a244704276962777b863fc2fb9', '127.0.0.1', '内网IP', '{ids=585e81a244704276962777b863fc2fb9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:12:31');
INSERT INTO `sys_oper_log` VALUES (197, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/7c14ad6110344334ad8ab8926e5a8b25', '127.0.0.1', '内网IP', '{ids=7c14ad6110344334ad8ab8926e5a8b25}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:12:37');
INSERT INTO `sys_oper_log` VALUES (198, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/bef0ba9d53c14a549869c87eace54f9a', '127.0.0.1', '内网IP', '{ids=bef0ba9d53c14a549869c87eace54f9a}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:12:40');
INSERT INTO `sys_oper_log` VALUES (199, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/016799b103d04f7ca5c91a887c671043', '127.0.0.1', '内网IP', '{ids=016799b103d04f7ca5c91a887c671043}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:12:45');
INSERT INTO `sys_oper_log` VALUES (200, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"%大长腿 %女神 %泳装美女 %美女 %国际超模大赛 %花絮 %泳池派对 %身材   https://v.douyin.com/RLguFSv/ 复制此链接，打开Dou音搜索，矗接觀kan視频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:15:07');
INSERT INTO `sys_oper_log` VALUES (201, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"0.25 FuS:/ %看一遍笑一遍 让你对象学学这精打细算  https://v.douyin.com/RLgt8eU/ 复制佌链接，da开Dou音搜索，矗接观看視频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:22:05');
INSERT INTO `sys_oper_log` VALUES (202, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"1.20 xSl:/ 二选一了 家人们@抖音小助手 %闺蜜写真 %模特 %水手服  https://v.douyin.com/RLp53ab/ 復制此链接，达开Dou音搜索，矗接觀看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:28:27');
INSERT INTO `sys_oper_log` VALUES (203, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"一个，你会怎么选？%拉拉队美女  %歌曲问情   https://v.douyin.com/RLpvy9M/ 复制此链接，打幵Dou音搜索，直接观看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:35:00');
INSERT INTO `sys_oper_log` VALUES (204, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"7.94 NjC:/ 和 @oc酒吧副总东东 一起 %合拍   https://v.douyin.com/RLsJAnH/ 鳆zhi佌lian接，打開Dou音搜索，直接观kan视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:36:17');
INSERT INTO `sys_oper_log` VALUES (205, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"4.38 kCU:/ %withoutme摇摆舞 绑头发版本怎么样可以吗  https://v.douyin.com/RLscJuF/ 复制此链接，打开Dou音搜索，直接觀看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:49:12');
INSERT INTO `sys_oper_log` VALUES (206, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"的，逃不掉的。”%国风合伙人 %异域风情 %异域国风美人天花板  https://v.douyin.com/RLsCNw6/ 复制此链接，打鐦Dou音搜索，直接觀看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 19:51:18');
INSERT INTO `sys_oper_log` VALUES (207, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"你来选。 %泳池 %身材  https://www.iesdouyin.com/share/video/7023596473258413316/?region=CN&mid=7023596468510444324&u_code=16a4fijf6&did=MS4wLjABAAAA1azrhCrz4w2OWz9XlnVcnki5lt8mw_R9WTHwKR1M42g3XClvZPVAI4qGiTbZydyZ&iid=MS4wLjABAAAA_loOOPQp3EVd0t4UOUqocPeHpGibz-e3jU-oTejfRuY&with_sec_did=1&titleType=title&utm_source=copy_link&utm_campaign=client_share&utm_medium=android&app=aweme&scheme_type=1 鳆淛此链接，打开Dou音搜索，矗接观看视频！\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-10-27 20:03:46');
INSERT INTO `sys_oper_log` VALUES (208, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"0.23 oqR:/ 外面零下8度 我怀里38度 你来选。 %泳池 %身材  https://v.douyin.com/RLGtVmP/ 鳆淛此链接，打开Dou音搜索，矗接观看视频\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 20:05:45');
INSERT INTO `sys_oper_log` VALUES (209, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"1.56 AgB:/   https://v.douyin.com/RLGGAfJ/ 复制此lian接，打kaiDou音搜索，直接观kan視频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 20:08:08');
INSERT INTO `sys_oper_log` VALUES (210, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"4.61 FHI:/ %抖音颜值 %可爱 %猫系女友 嘿嘿  https://v.douyin.com/RLtYwqD/ 复制Ci链接，打汧Douyin搜索，直接观看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 20:11:35');
INSERT INTO `sys_oper_log` VALUES (211, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"2.89 FUy:/ 不给腿买个保险都不敢出来跳舞%no9应援舞挑战  %no9 %no9舞蹈   https://v.douyin.com/RLt5NEf/ 復制此链接，打幵Dou吟搜索，矗接观看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-27 20:20:50');
INSERT INTO `sys_oper_log` VALUES (212, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”  https://v.douyin.com/RFE5nW5/ 复制此链接，打开Dou音搜索，直接观看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 18:47:56');
INSERT INTO `sys_oper_log` VALUES (213, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'loyj_douyin_class', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 19:10:40');
INSERT INTO `sys_oper_log` VALUES (214, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"loyj\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":26,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"分类主键ID\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1635505840000,\"tableId\":3,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Pid\",\"usableColumn\":false,\"columnId\":27,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pid\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"父类ID\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1635505840000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"pid\"},{\"capJavaField\":\"DyClassName\",\"usableColumn\":false,\"columnId\":28,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"dyClassName\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"抖音分类名称\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1635505840000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"dy_class_name\"},{\"capJavaField\":\"OrderNum\",\"usableColumn\":true,\"columnId\":29,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"orderNum\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"显示顺序\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(4)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1635505840000', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 19:12:20');
INSERT INTO `sys_oper_log` VALUES (215, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-10-29 19:12:39');
INSERT INTO `sys_oper_log` VALUES (216, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"loyj\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":26,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"分类主键ID\",\"updateTime\":1635505940000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1635505840000,\"tableId\":3,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Pid\",\"usableColumn\":false,\"columnId\":27,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pid\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"父类ID\",\"updateTime\":1635505940000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1635505840000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"pid\"},{\"capJavaField\":\"DyClassName\",\"usableColumn\":false,\"columnId\":28,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"dyClassName\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"抖音分类名称\",\"updateTime\":1635505940000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1635505840000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"dy_class_name\"},{\"capJavaField\":\"OrderNum\",\"usableColumn\":true,\"columnId\":29,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"orderNum\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"显示顺序\",\"updateTime\":1635505940000,\"sort\":4,\"list\":true,\"p', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 19:13:31');
INSERT INTO `sys_oper_log` VALUES (217, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-10-29 19:13:34');
INSERT INTO `sys_oper_log` VALUES (218, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"dyClassName\":\"测试\",\"pid\":\"0\",\"params\":{},\"children\":[],\"id\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 21:13:01');
INSERT INTO `sys_oper_log` VALUES (219, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"dyClassName\":\"测试2\",\"orderNum\":1,\"pid\":\"0\",\"params\":{},\"children\":[],\"id\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 21:14:53');
INSERT INTO `sys_oper_log` VALUES (220, '抖音分类', 3, 'com.ruoyi.loyj.controller.DouyinClassController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyinClass/2', '127.0.0.1', '内网IP', '{ids=2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:22:29');
INSERT INTO `sys_oper_log` VALUES (221, '抖音分类', 3, 'com.ruoyi.loyj.controller.DouyinClassController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyinClass/1', '127.0.0.1', '内网IP', '{ids=1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:22:32');
INSERT INTO `sys_oper_log` VALUES (222, '抖音分类', 3, 'com.ruoyi.loyj.controller.DouyinClassController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyinClass/3', '127.0.0.1', '内网IP', '{ids=3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:22:36');
INSERT INTO `sys_oper_log` VALUES (223, '抖音分类', 3, 'com.ruoyi.loyj.controller.DouyinClassController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyinClass/0', '127.0.0.1', '内网IP', '{ids=0}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:22:42');
INSERT INTO `sys_oper_log` VALUES (224, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"我的抖音\",\"orderNum\":1,\"pid\":\"0\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\idea工作集\\loyj-system\\loyj\\target\\classes\\mapper\\DouyinClassMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinClassMapper.insertDouyinClass-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin_class          ( pid,             dy_class_name,             order_num )           values ( ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-10-29 22:22:58');
INSERT INTO `sys_oper_log` VALUES (225, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"我的抖音\",\"orderNum\":1,\"pid\":\"0\",\"id\":\"a6da2056-c0b9-4d46-bf0e-ff64c3003430\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:24:47');
INSERT INTO `sys_oper_log` VALUES (226, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"美女\",\"orderNum\":1,\"pid\":\"a6da2056-c0b9-4d46-bf0e-ff64c3003430\",\"id\":\"6a62e9d2-433c-4201-8199-590431086924\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:24:59');
INSERT INTO `sys_oper_log` VALUES (227, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"风景\",\"orderNum\":2,\"pid\":\"0\",\"id\":\"d7627928-f260-4218-a7bb-c947acd17c96\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:25:08');
INSERT INTO `sys_oper_log` VALUES (228, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"测试\",\"orderNum\":1,\"pid\":\"a6da2056-c0b9-4d46-bf0e-ff64c3003430\",\"id\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:27:14');
INSERT INTO `sys_oper_log` VALUES (229, '抖音分类', 2, 'com.ruoyi.loyj.controller.DouyinClassController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"测试\",\"orderNum\":0,\"pid\":\"a6da2056-c0b9-4d46-bf0e-ff64c3003430\",\"id\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:27:21');
INSERT INTO `sys_oper_log` VALUES (230, '抖音分类', 3, 'com.ruoyi.loyj.controller.DouyinClassController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyinClass/d7627928-f260-4218-a7bb-c947acd17c96', '127.0.0.1', '内网IP', '{ids=d7627928-f260-4218-a7bb-c947acd17c96}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:27:32');
INSERT INTO `sys_oper_log` VALUES (231, '抖音分类', 2, 'com.ruoyi.loyj.controller.DouyinClassController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"风景\",\"orderNum\":2,\"pid\":\"a6da2056-c0b9-4d46-bf0e-ff64c3003430\",\"id\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:27:44');
INSERT INTO `sys_oper_log` VALUES (232, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"releaseTime\":1623509000,\"author\":\"依婷\",\"musicUrl\":\"https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6972921004717525773.mp3\",\"diggCount\":7186,\"params\":{},\"title\":\"#抖音颜值 #可爱 #猫系女友 嘿嘿\",\"commentCount\":161,\"musicTitle\":\"@依婷创作的原声\",\"shareCount\":130,\"createBy\":\"admin\",\"createTime\":1635336695000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0300fg10000c32cl7k0gv51pr7b5mt0&ratio=720p&line=0\",\"shareUrl\":\"6a62e9d2-433c-4201-8199-590431086924\",\"id\":\"ebc308c630f2494b85d673dd09d0ec5a\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-10-29 22:34:38');
INSERT INTO `sys_oper_log` VALUES (233, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"2.89 FUy:/ 不给腿买个保险都不敢出来跳舞%no9应援舞挑战  %no9 %no9舞蹈   https://v.douyin.com/RLt5NEf/ 復制此链接，打幵Dou吟搜索，矗接观看视频！\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\idea工作集\\loyj-system\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( id,                          share_url,             author,             title,             no_watermark_url,             release_time,             create_by,             music_title,             create_time,                          music_url,                                       comment_count,             digg_count,             share_count )           values ( ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ?,                                       ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\n; Field \'class_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'class_id\' doesn\'t have a default value', '2021-10-29 22:35:34');
INSERT INTO `sys_oper_log` VALUES (234, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"2.89 FUy:/ 不给腿买个保险都不敢出来跳舞%no9应援舞挑战  %no9 %no9舞蹈   https://v.douyin.com/RLt5NEf/ 復制此链接，打幵Dou吟搜索，矗接观看视频！\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\idea工作集\\loyj-system\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( id,                          share_url,             author,             title,             no_watermark_url,             release_time,             create_by,             music_title,             create_time,                          music_url,                                       comment_count,             digg_count,             share_count )           values ( ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ?,                                       ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\n; Field \'class_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'class_id\' doesn\'t have a default value', '2021-10-29 22:38:25');
INSERT INTO `sys_oper_log` VALUES (235, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”  https://v.douyin.com/RFE5nW5/ 复制此链接，打开Dou音搜索，直接观看视频\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\idea工作集\\loyj-system\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( id,                          share_url,             author,             title,             no_watermark_url,             release_time,             create_by,             music_title,             create_time,                          music_url,                                       comment_count,             digg_count,             share_count )           values ( ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ?,                                       ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\n; Field \'class_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'class_id\' doesn\'t have a default value', '2021-10-29 22:38:41');
INSERT INTO `sys_oper_log` VALUES (236, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”  https://v.douyin.com/RFE5nW5/ 复制此链接，打开Dou音搜索，直接观看视频\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\idea工作集\\loyj-system\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( id,                          share_url,             author,             title,             no_watermark_url,             release_time,             create_by,             music_title,             create_time,                          music_url,                                       comment_count,             digg_count,             share_count )           values ( ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ?,                                       ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\n; Field \'class_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'class_id\' doesn\'t have a default value', '2021-10-29 22:38:52');
INSERT INTO `sys_oper_log` VALUES (237, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"shareUrl\":\"茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”  https://v.douyin.com/RFE5nW5/ 复制此链接，打开Dou音搜索，直接观看视频\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\idea工作集\\loyj-system\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( id,                          share_url,             author,             title,             no_watermark_url,             release_time,             create_by,             music_title,             create_time,                          music_url,                                       comment_count,             digg_count,             share_count )           values ( ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ?,                                       ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\n; Field \'class_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'class_id\' doesn\'t have a default value', '2021-10-29 22:39:46');
INSERT INTO `sys_oper_log` VALUES (238, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\",\"shareUrl\":\"茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”  https://v.douyin.com/RFE5nW5/ 复制此链接，打开Dou音搜索，直接观看视频\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\idea工作集\\loyj-system\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( id,                          share_url,             author,             title,             no_watermark_url,             release_time,             create_by,             music_title,             create_time,                          music_url,                                       comment_count,             digg_count,             share_count )           values ( ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ?,                                       ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\n; Field \'class_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'class_id\' doesn\'t have a default value', '2021-10-29 22:40:48');
INSERT INTO `sys_oper_log` VALUES (239, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\",\"shareUrl\":\"茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”  https://v.douyin.com/RFE5nW5/ 复制此链接，打开Dou音搜索，直接观看视频\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\idea工作集\\loyj-system\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( id,                          share_url,             author,             title,             no_watermark_url,             release_time,             create_by,             music_title,             create_time,                          music_url,                                       comment_count,             digg_count,             share_count )           values ( ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ?,                                       ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\n; Field \'class_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'class_id\' doesn\'t have a default value', '2021-10-29 22:42:24');
INSERT INTO `sys_oper_log` VALUES (240, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\",\"shareUrl\":\"茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”  https://v.douyin.com/RFE5nW5/ 复制此链接，打开Dou音搜索，直接观看视频\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\idea工作集\\loyj-system\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( id,                          share_url,             author,             title,             no_watermark_url,             release_time,             create_by,             music_title,             create_time,                          music_url,                                       comment_count,             digg_count,             share_count )           values ( ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ?,                                       ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'class_id\' doesn\'t have a default value\n; Field \'class_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'class_id\' doesn\'t have a default value', '2021-10-29 22:43:56');
INSERT INTO `sys_oper_log` VALUES (241, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\",\"shareUrl\":\"茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”  https://v.douyin.com/RFE5nW5/ 复制此链接，打开Dou音搜索，直接观看视频\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 22:48:29');
INSERT INTO `sys_oper_log` VALUES (242, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/44f0554727fb424c8b87ed0ec6228dd7', '127.0.0.1', '内网IP', '{ids=44f0554727fb424c8b87ed0ec6228dd7}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 23:08:43');
INSERT INTO `sys_oper_log` VALUES (243, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"author\":\"新华网\",\"musicUrl\":\"\",\"diggCount\":1566387,\"params\":{},\"title\":\"国社小姐姐改词翻唱《后妈茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”\",\"commentCount\":58448,\"musicTitle\":\"@新华网创作的原声\",\"shareCount\":169160,\"classId\":\"6a62e9d2-433c-4201-8199-590431086924\",\"createBy\":\"admin\",\"createTime\":1635504476000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c5tjsn3c77ueachdm8u0&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/RFE5nW5/\",\"id\":\"4bf75150b5b949d08196fa10ca6f83ef\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 23:30:36');
INSERT INTO `sys_oper_log` VALUES (244, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"author\":\"时尚美图\",\"musicUrl\":\"https://sf6-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/d99a4a9491684953adb9d8201d8efa32\",\"diggCount\":15,\"params\":{},\"title\":\"#美女图片 #高清 #身材 \",\"commentCount\":0,\"musicTitle\":\"Trip\",\"shareCount\":10,\"classId\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\",\"createBy\":\"admin\",\"createTime\":1634920236000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=https://sf6-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/d99a4a9491684953adb9d8201d8efa32&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/RJ1eA3d/\",\"id\":\"07c7eeed329141b2bc586d782a65856e\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 23:36:21');
INSERT INTO `sys_oper_log` VALUES (245, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"author\":\"新华网\",\"musicUrl\":\"\",\"diggCount\":2388213,\"updateTime\":1635521437000,\"params\":{},\"title\":\"国社小姐姐改词翻唱《后妈茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”\",\"commentCount\":84788,\"musicTitle\":\"@新华网创作的原声\",\"shareCount\":220826,\"classId\":\"6a62e9d2-433c-4201-8199-590431086924\",\"createBy\":\"admin\",\"createTime\":1635504476000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c5tjsn3c77ueachdm8u0&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/RFE5nW5/\",\"id\":\"4bf75150b5b949d08196fa10ca6f83ef\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 23:36:29');
INSERT INTO `sys_oper_log` VALUES (246, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"author\":\"遇见谁\",\"musicUrl\":\"https://sf3-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/d99a4a9491684953adb9d8201d8efa32\",\"diggCount\":19,\"updateTime\":1635521782000,\"params\":{},\"title\":\"#美女图片 #高清 #身材 \",\"commentCount\":2,\"musicTitle\":\"Trip\",\"shareCount\":14,\"classId\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\",\"createBy\":\"admin\",\"createTime\":1634920236000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=https://sf3-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/d99a4a9491684953adb9d8201d8efa32&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/RJ1eA3d/\",\"id\":\"07c7eeed329141b2bc586d782a65856e\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 23:38:17');
INSERT INTO `sys_oper_log` VALUES (247, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"author\":\"新华网\",\"musicUrl\":\"\",\"diggCount\":2397337,\"updateTime\":1635521789000,\"params\":{},\"title\":\"国社小姐姐改词翻唱《后妈茶话会》，讲述两年来的中美抗疫差距，网友直呼：“太炸了！”\",\"commentCount\":84956,\"musicTitle\":\"@新华网创作的原声\",\"shareCount\":221522,\"classId\":\"6a62e9d2-433c-4201-8199-590431086924\",\"createBy\":\"admin\",\"createTime\":1635504476000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=v0200fg10000c5tjsn3c77ueachdm8u0&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/RFE5nW5/\",\"id\":\"4bf75150b5b949d08196fa10ca6f83ef\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 23:39:13');
INSERT INTO `sys_oper_log` VALUES (248, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"6a62e9d2-433c-4201-8199-590431086924\",\"shareUrl\":\"蹈   https://v.douyin.com/RLt5NEf/ 復制此链接，打幵Dou吟搜索，矗接观看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 23:39:41');
INSERT INTO `sys_oper_log` VALUES (249, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/a5171ccc9ce64db09ef2d86714b76060', '127.0.0.1', '内网IP', '{ids=a5171ccc9ce64db09ef2d86714b76060}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-29 23:40:09');
INSERT INTO `sys_oper_log` VALUES (250, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"edit\",\"orderNum\":\"7\",\"menuName\":\"参数设置\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"config\",\"component\":\"system/config/index\",\"children\":[],\"createTime\":1631286482000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":106,\"menuType\":\"C\",\"perms\":\"system:config:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 10:27:53');
INSERT INTO `sys_oper_log` VALUES (251, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\",\"shareUrl\":\"0.20 pqe:/ %狗头   https://v.douyin.com/RYbELGb/ 复制此链接，咑开Dou搜索，直接观看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 10:35:59');
INSERT INTO `sys_oper_log` VALUES (252, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/loyj_douyin_photo', '127.0.0.1', '内网IP', '{tableName=loyj_douyin_photo}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 11:05:39');
INSERT INTO `sys_oper_log` VALUES (253, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"loyj\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":23,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键ID\",\"updateTime\":1635249750000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1634916436000,\"tableId\":2,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"DouyinId\",\"usableColumn\":false,\"columnId\":30,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"douyinId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"抖音外键ID\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1635563139000,\"isEdit\":\"1\",\"tableId\":2,\"pk\":false,\"columnName\":\"douyin_id\"},{\"capJavaField\":\"PhotoUrl\",\"usableColumn\":false,\"columnId\":31,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"photoUrl\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"照片链接\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"\",\"isPk\":\"0\",\"createTime\":1635563139000,\"isEdit\":\"1\",\"tableId\":2,\"pk\":false,\"columnName\":\"photo_url\"}],\"businessName\":\"photo\",\"moduleName\":\"loyj\",\"className\":\"DouyinPhoto\",\"tableName\":\"loyj_douyin_photo\",\"crud\":true,\"options\":\"{}\",\"genType\":\"0\",\"packageName\":\"com.ruoyi.loyj\",\"functionName\":\"抖音照片\",\"tree\":false,\"tableComment\":\"抖音照片表\",\"params\":{},\"tplCategory\":\"crud\",\"tableId\":2,\"genPath\":\"/\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 11:06:02');
INSERT INTO `sys_oper_log` VALUES (254, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-10-30 11:06:07');
INSERT INTO `sys_oper_log` VALUES (255, '抖音解析', 2, 'com.ruoyi.loyj.controller.DouyinController.edit()', 'PUT', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"author\":\"遇见谁\",\"musicUrl\":\"https://sf6-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/d99a4a9491684953adb9d8201d8efa32\",\"diggCount\":19,\"updateTime\":1635521898000,\"params\":{},\"title\":\"#美女图片 #高清 #身材 \",\"commentCount\":2,\"musicTitle\":\"Trip\",\"shareCount\":14,\"classId\":\"e0c8a90d-c922-4416-8c3c-37eb844d983a\",\"createBy\":\"admin\",\"createTime\":1634920236000,\"updateBy\":\"admin\",\"noWatermarkUrl\":\"https://aweme.snssdk.com/aweme/v1/play/?video_id=https://sf6-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/d99a4a9491684953adb9d8201d8efa32&ratio=720p&line=0\",\"shareUrl\":\"https://v.douyin.com/RJ1eA3d/\",\"id\":\"07c7eeed329141b2bc586d782a65856e\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 11:14:46');
INSERT INTO `sys_oper_log` VALUES (256, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/07c7eeed329141b2bc586d782a65856e', '127.0.0.1', '内网IP', '{ids=07c7eeed329141b2bc586d782a65856e}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 11:14:49');
INSERT INTO `sys_oper_log` VALUES (257, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"6a62e9d2-433c-4201-8199-590431086924\",\"shareUrl\":\"https://v.douyin.com/RJ1eA3d/\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 11:14:56');
INSERT INTO `sys_oper_log` VALUES (258, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/c659deb36665479fafd54d40cdec16b8', '127.0.0.1', '内网IP', '{ids=c659deb36665479fafd54d40cdec16b8}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 11:16:41');
INSERT INTO `sys_oper_log` VALUES (259, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"6a62e9d2-433c-4201-8199-590431086924\",\"shareUrl\":\"https://v.douyin.com/RJ1eA3d/\"}', 'null', 1, 'nested exception is org.apache.ibatis.binding.BindingException: Parameter \'douyinId\' not found. Available parameters are [arg0, collection, list]', '2021-10-30 11:16:46');
INSERT INTO `sys_oper_log` VALUES (260, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"6a62e9d2-433c-4201-8199-590431086924\",\"shareUrl\":\"4.69 QKW:/ 自取%图集 %图集壁纸   https://v.douyin.com/RYG26Kw/ 复zhi佌鏈接，打\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 11:20:14');
INSERT INTO `sys_oper_log` VALUES (261, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"搞笑\",\"orderNum\":3,\"pid\":\"a6da2056-c0b9-4d46-bf0e-ff64c3003430\",\"id\":\"97aa1525-ec98-4031-aa77-c0e4735a4fff\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 15:13:09');
INSERT INTO `sys_oper_log` VALUES (262, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"张三\",\"orderNum\":1,\"pid\":\"6a62e9d2-433c-4201-8199-590431086924\",\"id\":\"dba39222-deb8-43c4-845c-d84efc72dc26\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 15:26:28');
INSERT INTO `sys_oper_log` VALUES (263, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"dba39222-deb8-43c4-845c-d84efc72dc26\",\"shareUrl\":\"看一遍笑一遍 %一定要看到最后 %艾特你想艾特的人  https://v.douyin.com/RYwsEyn/ 复製此链接，打开Dou荫搜索，直接观kan视頻\"}', 'null', 1, '', '2021-10-30 15:27:06');
INSERT INTO `sys_oper_log` VALUES (264, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"dba39222-deb8-43c4-845c-d84efc72dc26\",\"shareUrl\":\"看一遍笑一遍 %一定要看到最后 %艾特你想艾特的人  https://v.douyin.com/RYwsEyn/ 复製此链接，打开Dou荫搜索，直接观kan视頻\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 15:28:36');
INSERT INTO `sys_oper_log` VALUES (265, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"李四\",\"orderNum\":1,\"pid\":\"dba39222-deb8-43c4-845c-d84efc72dc26\",\"id\":\"9083fe85-376a-4e6c-8d90-eb80780bc3ad\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-10-30 16:12:37');
INSERT INTO `sys_oper_log` VALUES (266, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"97aa1525-ec98-4031-aa77-c0e4735a4fff\",\"shareUrl\":\"5.66 Ljc:/ 出来单挑啊%出来单挑  https://v.douyin.com/RAyWfQX/ 复zhi此链接，哒開Dou音搜索，矗接觀kan视频！\"}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'no_watermark_url\' at row 1\r\n### The error may exist in file [E:\\idea工作集\\loyj-system\\loyj\\target\\classes\\mapper\\DouyinMapper.xml]\r\n### The error may involve com.ruoyi.loyj.mapper.DouyinMapper.insertDouyin-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into loyj_douyin          ( id,             class_id,             share_url,             author,             title,             no_watermark_url,             create_by,             music_title,             create_time,                          music_url,                                       comment_count,             digg_count,             share_count )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ?,                                       ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'no_watermark_url\' at row 1\n; Data truncation: Data too long for column \'no_watermark_url\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'no_watermark_url\' at row 1', '2021-11-06 20:03:16');
INSERT INTO `sys_oper_log` VALUES (267, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"97aa1525-ec98-4031-aa77-c0e4735a4fff\",\"shareUrl\":\"出来单挑啊%出来单挑  https://v.douyin.com/RAyWfQX/ 复zhi此链接，哒開Dou音搜索，矗接觀kan视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-06 20:04:15');
INSERT INTO `sys_oper_log` VALUES (268, '抖音分类', 3, 'com.ruoyi.loyj.controller.DouyinClassController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyinClass/9083fe85-376a-4e6c-8d90-eb80780bc3ad', '127.0.0.1', '内网IP', '{ids=9083fe85-376a-4e6c-8d90-eb80780bc3ad}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-06 20:04:57');
INSERT INTO `sys_oper_log` VALUES (269, '抖音分类', 3, 'com.ruoyi.loyj.controller.DouyinClassController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyinClass/dba39222-deb8-43c4-845c-d84efc72dc26', '127.0.0.1', '内网IP', '{ids=dba39222-deb8-43c4-845c-d84efc72dc26}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-06 20:05:01');
INSERT INTO `sys_oper_log` VALUES (270, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"酷酷的\",\"orderNum\":4,\"pid\":\"a6da2056-c0b9-4d46-bf0e-ff64c3003430\",\"id\":\"7c37cf38-30d6-4076-9a6d-53688de25e31\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-06 20:05:20');
INSERT INTO `sys_oper_log` VALUES (271, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"7c37cf38-30d6-4076-9a6d-53688de25e31\",\"shareUrl\":\"9.76 oDH:/ “不需要你看上我，我根本看不见你”  https://v.douyin.com/RAf13Lv/ 复製此lian接，哒开Dou音搜索，直接观看视pin！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-06 20:05:27');
INSERT INTO `sys_oper_log` VALUES (272, '抖音解析', 3, 'com.ruoyi.loyj.controller.DouyinController.remove()', 'DELETE', 1, 'admin', NULL, '/spider/douyin/212960148dc24361872135374607c20f', '127.0.0.1', '内网IP', '{ids=212960148dc24361872135374607c20f}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-06 20:08:32');
INSERT INTO `sys_oper_log` VALUES (273, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"97aa1525-ec98-4031-aa77-c0e4735a4fff\",\"shareUrl\":\"5.10 wfO:/ %搞笑视频   https://v.douyin.com/RA5ebBt/ 復制此鏈接，咑开Dou音搜索，直接观看视頻！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-06 20:19:10');
INSERT INTO `sys_oper_log` VALUES (274, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"97aa1525-ec98-4031-aa77-c0e4735a4fff\",\"shareUrl\":\"3.89 Slp:/ 7月30日，是郭豪牺牲两周年！让我们铭记这个年轻的士兵！ %致敬英雄  %烈士  https://v.douyin.com/RA5Mjqd/ 复制此链接，哒开Dou音搜索，直接觀看视频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-06 20:26:17');
INSERT INTO `sys_oper_log` VALUES (275, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"97aa1525-ec98-4031-aa77-c0e4735a4fff\",\"shareUrl\":\"%不回消息表情包 %表情包 %抖音小助手   https://v.douyin.com/RUBnCQY/ 复制佌链接，打开Dou愔搜索，矗接观看视頻！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-08 09:02:35');
INSERT INTO `sys_oper_log` VALUES (276, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"97aa1525-ec98-4031-aa77-c0e4735a4fff\",\"shareUrl\":\"WestCoast   .%OldSchool  https://v.douyin.com/RyvDFUS/ fu制佌链接，da幵Dou音搜索，直接观看視频！\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-09 17:23:44');
INSERT INTO `sys_oper_log` VALUES (277, '抖音分类', 1, 'com.ruoyi.loyj.controller.DouyinClassController.add()', 'POST', 1, 'admin', NULL, '/spider/douyinClass', '127.0.0.1', '内网IP', '{\"children\":[],\"dyClassName\":\"好音乐\",\"orderNum\":5,\"pid\":\"0\",\"id\":\"36cab16f-8a85-4055-9ef1-27bd875a6de0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-10 10:02:18');
INSERT INTO `sys_oper_log` VALUES (278, '抖音解析', 1, 'com.ruoyi.loyj.controller.DouyinController.add()', 'POST', 1, 'admin', NULL, '/spider/douyin', '127.0.0.1', '内网IP', '{\"params\":{},\"classId\":\"36cab16f-8a85-4055-9ef1-27bd875a6de0\",\"shareUrl\":\"了%超燃bgm %音乐 %超燃   https://v.douyin.com/RyYLKHc/ 復zhi此链接，打开Dou茵搜索，直接觀看视频\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-11-10 10:02:35');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
                             `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
                             `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位编码',
                             `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位名称',
                             `post_sort` int(4) NOT NULL COMMENT '显示顺序',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '状态（0正常 1停用）',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2021-09-10 23:08:02', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2021-09-10 23:08:02', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                             `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
                             `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
                             `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色权限字符串',
                             `role_sort` int(4) NOT NULL COMMENT '显示顺序',
                             `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
                             `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
                             `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2021-09-10 23:08:02', '', NULL, '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
                                  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
                                  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
                                  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
                                  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
                             `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
                             `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户账号',
                             `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户昵称',
                             `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
                             `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户邮箱',
                             `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '手机号码',
                             `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
                             `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '头像地址',
                             `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '密码',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                             `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '最后登录IP',
                             `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2021-11-10 14:34:11', 'admin', '2021-09-10 23:08:02', '', '2021-11-10 14:34:10', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2021-09-10 23:08:02', 'admin', '2021-09-10 23:08:02', '', NULL, '测试员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
                                  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
                                  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
                                  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
                                  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
                                  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
