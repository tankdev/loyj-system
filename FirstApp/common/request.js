const baseUrl = "http://loyj.vaiwan.com/uniapp";

const httpRequest = (url, method, data) => {
	
	let meth = method.toUpperCase();//小写改为大写
	if (!data) {
		data = {}
	}
	
	
    let httpDefaultOpts = {
        url: baseUrl + url,
        data: data,
        method: meth,
		header:{
			'X-Requested-With': 'XMLHttpRequest',
			'content-type': 'application/json',
		},
        dataType: 'json',
    }
    return new Promise(function(resolve, reject) {
        uni.request(httpDefaultOpts).then(
            (res) => {
				if(res[1].statusCode == 200){//成功返回
					resolve(res[1])
				}else{//错误信息
					uni.showToast({
						title:res[1].errMsg,
						icon:'none'
					})
                    //resolve(res[1]) //错误信息返回
				}
            }
        ).catch(
            (response) => {
                reject(response)
            }
        )
    })
};

let request = require("request");
const spiderRequirst = (options) => {
	let op = Object.assign(
	    {},
	    {
	      url: "",
	      method: "GET",
	      encoding: null,
	      header: {
	        "User-Agent":
	          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
	        Referer: "https://www.meituri.com"
	      }
	    },
	    options
	  );
	
	  if (op.url === "") {
	    throw new Error("请求的url地址不正确");
	  }
	
	  const promise = new Promise(function(resolve, reject) {
	    request(op, (err, response, body) => {
	      if (err) reject(err);
	
	      if (response && response.statusCode === 200) {
	        resolve(body);
	      } else {
	        reject(`请求✿✿✿${url}✿✿✿失败`);
	      }
	    });
	  });
	
	  return promise;
}






export default {
	baseUrl,
	httpRequest				
}

