
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import backTop from 'components/back-top/back-top.vue'
import fuiToast from "components/fui-toast/fui-toast"
import http from 'common/request.js'
import dateBack from 'common/utils.js'

Vue.component('back-top',backTop)
Vue.component('message', fuiToast)

Vue.prototype.http = http
Vue.prototype.dateBack = dateBack
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif